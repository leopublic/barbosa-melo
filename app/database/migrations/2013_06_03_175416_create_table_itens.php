<?php

use Illuminate\Database\Migrations\Migration;

class CreateTableItens extends Migration {
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('itens', function($table)
		{
			$table->engine = 'InnoDB';
			$table->increments('id');
			$table->integer('unidade_id')->unsigned()->nullable();
			$table->string('codigo',30)->unique();
			$table->string('descricao', 2000);
			$table->timestamps();
		});	
		Schema::table('itens', function($table)
		{
			$table->foreign('unidade_id')->references('id')->on('unidades');
		});	
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('itens');
	}
}