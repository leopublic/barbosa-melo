<?php
use Illuminate\Database\Migrations\Migration;
class CreateTableEspec extends Migration {
	public function up()
	{
		Schema::create('especs', function($table) {
			$table->increments('id');
			$table->string('descricao');
			$table->timestamps();
		});
	}

	public function down()
	{
		Schema::drop('especs');
	}
}