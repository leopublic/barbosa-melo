@extends('layout')

@section('conteudo')
<div class="interno">
	<div id="header">
		<div class="acoes">
		</div>
		<h1>Importações de saldo de estoque</h1>
	</div>
	<div class="form">
	{{ Form::open(array('url' => 'importacaoestoque', 'files' => true)) }}
		<ul>
			<li>
				<p class="label">Informe o arquivo a ser importado:</p>
				<p class="content">{{ Form::file('arquivo',array('id'=> '0')); }}</p>
			</li>
		</ul>
	<div style="text-align: center;margin: auto;">
		{{ Form::submit('Enviar'); }}
	</div>
	@if (Session::has('msg'))
		<span class="error">{{ Session::get('msg') }}</span>
	@endif	
	{{ Form::close() }}	
	<table class="listagem">
		<thead>
			<tr>
				<th class="c">Ação</th>
				<th>Data importação</th>
				<th>Arquivo</th>
			</tr>
		</thead>
		<tbody>
			@foreach ($importacoes as $importacao)
			<tr>
				<td class="c"><a href="importacaoestoque/importe/{{ $importacao->id }}"><i class="icon-magic"></i></a> </td>
				<td>{{ $importacao->created_at }}</td>
				<td>{{ $importacao->nome_arquivo }}</td>
			</tr>
			@endforeach
		</tbody>
	</table>
	
	</div>
</div>
@stop