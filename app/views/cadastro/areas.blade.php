@extends('layout')

@section('conteudo')
<div class="interno">
	<h1>Áreas</h1>
	{{ Form::open() }}
	<table>
		@foreach ($areas as $area)
		<tr><td>{{ $area->id }}</td><td>{{ $area->descricao }}</td></tr>
		@endforeach
	</table>
	{{ Form::close() }}	
</div>
@stop