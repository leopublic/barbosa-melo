<?php

use Illuminate\Database\Migrations\Migration;

class AlterTableItensRecebidos extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('itens_recebidos', function($table)
		{
			$table->decimal('qtd', 11, 2);
			$table->integer('item_id')->unsigned()->nullable();
			$table->foreign('item_id')->references('id')->on('itens');
			$table->integer('recebimento_id')->unsigned()->nullable();
			$table->foreign('recebimento_id')->references('id')->on('recebimentos');
		});	
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
	}

}