<?php
Route::controller('auth', 'AuthController');
Route::get('logout', function(){Auth::logout(); return Redirect::to('/');});

Route::group(array('before' => 'auth'), function(){
	Route::get('/', function()
	{
		return View::make('hello');
	});
	Route::controller('cadastro/area'		, 'AreaController');
	Route::controller('rm'					, 'RequisicaoMaterialController');
	Route::controller('item'				, 'ItemController');
	Route::controller('ordemcompra'			, 'OrdemCompraController');
	Route::controller('recebimento'			, 'RecebimentoController');
	Route::controller('spool'				, 'SpoolController');
	Route::controller('isometrico'			, 'IsometricoController');
	Route::controller('user'				, 'UserController');
	Route::controller('importacaospool'		, 'ImportacaoSpoolController');
	Route::controller('importacaorm'		, 'ImportacaoRmController');
	Route::controller('importacaorec'		, 'ImportacaoRecController');
	Route::controller('importacaooc'		, 'ImportacaoOcController');
	Route::controller('importacaoiso'		, 'ImportacaoIsometricoController');
	Route::controller('importacaoestoque'	, 'ImportacaoEstoqueController');
	Route::controller('materiais'			, 'ItemController');
});