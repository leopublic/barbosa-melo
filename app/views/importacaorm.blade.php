@extends('layout')

@section('conteudo')
<div class="interno">
	<div id="header">
		<div class="acoes">
		</div>
		<h1>Carregar requisições de material</h1>
	</div>
	<div class="form">
	{{ Form::open(array('url' => 'ImportacaoRm/Novo', 'files' => true)) }}
		<ul>
			<li>
				<p>Informe o arquivo a ser importado:</p>{{ Form::file('arquivo',array('id'=> '0')); }}
			</li>
		</ul>
	<div style="text-align: center;margin: auto;">
		{{ Form::submit('Enviar'); }}
	</div>
	@if (Session::has('msg'))
		<span class="error">{{ Session::get('msg') }}</span>
	@endif	
	{{ Form::close() }}	
	<table class="listagem">
		<thead>
			<tr>
				<th>Ação</th>
				<th>Data importação</th>
				<th>Arquivo</th>
			</tr>
		</thead>
		<tbody>
			@foreach ($importacoes as $importacao)
			<tr>
				<td><a href="importacaorm/importe/{{ $importacao->id }}"><i class="icon-magic"></i></a> </td>
				<td>{{ $importacao->created_at }}</td>
				<td>{{ $importacao->nome_arquivo }}</td>
			</tr>
			@endforeach
		</tbody>
	</table>
	
	</div>
</div>
@stop