@extends('layout')

@section('conteudo')
<div class="interno">
	<div id="header">
		<div class="acoes">
			<a href="{{ URL::to('ImportacaoRm') }}"><i class="icon-upload-alt"></i></a>
		</div>
		<h1>Requisições de material</h1>		
	</div>
	{{ Form::open() }}	
	<div class="filtro">
		<ul>
			<li>
				<p>RM:</p>{{ Form::select('rm_id', $rms, Input::old('rm_id')) }}
			</li>
		</ul>	
		{{ Form::submit('Filtrar') }}
	</div>
	{{ Form::close() }}	
	{{ Form::open() }}
	<table class="listagem">
		<thead>
			<tr>
				<th>Código</th>
				<th>Descrição</th>
				<th class="r">Qtd solicitada</th>
				<th class="r">Qtd pedida</th>
				<th class="r">Qtd recebida</th>
			</tr>
		</thead>
		@if (isset($itens_solicitados))
		<tbody>
			@foreach ($itens_solicitados as $itemsol)
			<tr>
				<td>{{ $itemsol->item->codigo }}</td>
				<td>{{ $itemsol->item->descricao }}</td>
				<td class="r">{{ $itemsol->qtd }}</td>
				<td class="r">{{ $itemsol->item->qtdComprada() }}</td>
				<td class="r">{{ $itemsol->item->qtdRecebida() }}</td>
			</tr>
			@endforeach
		</tbody>
		@endif
	</table>
	{{ Form::close() }}	
</div>
@stop