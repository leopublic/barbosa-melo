@extends('layout')

@section('conteudo')
<div class="interno">
	<h1>Receber Ordem de Compra {{ $ordem->numero }}</h1>
	{{ Form::open() }}
	<table class="listagem">
		<thead>
			<tr>
				<th>Código</th>
				<th>Descrição</th>
				<th class="r">Qtd pedida</th>
				<th>Qtd recebida</th>
			</tr>
		</thead>
		<tbody>
			@foreach ($itenscomprados as $itemcomprado)
			<tr>
				<td>{{ $itemcomprado->item->codigo }}</td>
				<td>{{ $itemcomprado->item->descricao }}</td>
				<td class="r">{{ $itemcomprado->qtd }}</td>
				<td><input type="text"/></td>
			</tr>
			@endforeach			
		</tbody>
	</table>
	<div style="text-align: center;">
		<input type="submit" value="Receber"/>		
	</div>
	{{ Form::close() }}	
</div>
@stop