<?php

use Illuminate\Database\Migrations\Migration;

class AlterTableArmazens extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('armazens', function($table)
		{
			$table->integer('local_id')->unsigned()->nullable();
			$table->foreign('local_id')->references('id')->on('locais');
		});	
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
	}

}