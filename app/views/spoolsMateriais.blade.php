@extends('layout')

@section('conteudo')
<div class="interno">
	{{ Form::open(array("name" => "form", "id" => "form")) }}
	<div class="grid">
		<div class="row">
			<h1 class="slot-0-1-2-3">Programação de Fabricação de Spools</h1>
			<p class="slot-4-5" style="margin: 0; text-align:right;">
				<i class="icon-search" style="background-color: #00b0ca; padding-left:25px; padding-right:25px; padding-top:5px; padding-bottom:5px; color:#fff; cursor:pointer;" onclick="$('#form').submit();" title="Clique para aplicar os filtros"></i>
			</p>

		</div>
		<div class="row" style="margin-top:10px;">
			<h2>Spools sem data de visual de solda no ControlTub de isométricos com status "E - Liberado para produção" e sem pendências no controle da SEI</h2>
		</div>
	</div>	
	<div class="grid filtro">
		<div class="row">
			<p class="slot-0-1" style="margin-top:20px;">Status:<br/>{{ Form::select('status_spool_id', $status_spool, Input::old('status_spool_id')) }}</p>
			<p class="slot-2-3" style="margin-top:20px;">Unidade:<br/>{{ Form::select('unidade_prod_id', array('' => '(todas)') + $unidades_prod, Input::old('unidade_prod_id')) }}</p>
			<p class="slot-4-5" style="margin-top:20px;">Somente com saldo:<br/>{{ Form::checkbox('so_com_saldo', '1', Input::old('so_com_saldo')) }}</p>
		</div>
		<div class="row">
			<p class="slot-0-1" style="margin-top:20px;">Isométrico:<br/>{{ Form::text('codigo', Input::old('codigo')) }}</p>
		</div>
	</div>		
	
	@if (isset($spools)) 
	<table class="listagem">
		<colgroup>
			<col width="70px">
			<col width="250px">
			<col width="100px">
			<col width="130px">
			<col width="auto">
			<col width="80px">
			<col width="80px">
			<col width="80px">
			<col width="80px">
		</colgroup>
		<thead>
		<tr>
			<th class="c">Ações</th>
			<th style="text-align:left;">
					Spool
					<span style="color: #dadada;">
					<br/><i class="icon-angle-right" style="margin-left:7px;margin-right:2px;"></i>Linha
					<br/><i class="icon-angle-right" style="margin-left:14px;margin-right:2px;"></i>Isométrico
					</span>
			</th>
			<th class="r">
				Peso(kg)
				@if (isset($peso_total)) 
					<br/>(tot. {{ $peso_total }})
				@endif 
			</th>
			<th>Item</th>
			<th>Descrição</th>
			<th class="r">Qtd projeto</th>
			<th class="r">Qtd RM</th>
			<th class="r">Qtd comprada</th>
			<th class="r">Saldo em estoque</th>
		</tr>			
		</thead>	
		<tbody>
		<?php $spool_ant = ''; ?>
		@foreach ($spools as $spool)
		<?php $spool_ant = $spool->id; ?>
		<tr style="border-top: solid 2px #777;">
			<td class="iso c" rowspan="{{ $spool->QtdItensDoSpool() }}" >
			@foreach ($spool->acao() as $acao)				
				<a href="#" data-parameters="{{{ json_encode($acao['data']) }}}" onclick="javascript:AcionaProcesso(this);">{{ $acao['titulo'] }}</a>
			@endforeach			
			</td>
			<td  rowspan="{{ $spool->QtdItensDoSpool() }}" style="text-align:left;">
					{{ $spool->codigo }}
					<span style="color: #dadada;">
						<br/><i class="icon-angle-right" style="margin-left:7px;margin-right:2px;"></i>{{ $spool->linha->codigo }}
						<br/><i class="icon-angle-right" style="margin-left:14px;margin-right:2px;"></i>{{ $spool->linha->isometrico->codigo }}
					</span>
			</td>
			<td class="iso r" rowspan="{{ $spool->QtdItensDoSpool() }}">{{ number_format($spool->peso, 2, ",", ".") }}</td>
			@foreach ($spool->ItensDoSpool() as $item)
				<td>{{ $item->codigo }}</td>
				<td>{{ $item->descricao }}</td>
				<td class="r">{{ number_format($item->qtd, 2, ",", ".") }}</td>
				<td class="r">{{ number_format($item->qtd_solicitada, 2, ",", ".") }}</td>
				<td class="r">{{ number_format($item->qtd_comprada, 2, ",", ".") }}</td>
				<td class="r">{{ number_format($item->saldo, 2, ",", ".") }}</td>
			</tr>
			@endforeach			
		@endforeach			
			
		</tbody>
	</table>
	@else
	Selecione uma unidade
	@endif
	{{ Form::close() }}	
</div>
@stop