<?php
/**
 * @property int $id Chave
 * @property string $descricao Descricao
 */
class Linha extends Eloquent{
	protected $table = 'linhas';

	public function isometrico(){
		return $this->belongsTo('Isometrico', 'isometrico_id');
	}
	
	public function spool(){
		return $this->hasMany('Spool');
	}

	/**
	 * Cadastra uma nova ou recupera se houver. 
	 * Utilizado nas importacoes
	 * @param string $pdescricao Descricao da area que se deseja cadastrar.
	 * @return Area Retorna uma instancia de Area
	 */
	public static function Valido($pcodigo, $pisometrico_id)
	{
		$pcodigo = strtoupper($pcodigo);
		if($pcodigo == ''){
			$obj = new Linha();
		}
		else{
			$obj = Linha::where('codigo', '=', $pcodigo)->first();
			if(!isset($obj)){
				$obj = new Linha();
				$obj->isometrico_id = $pisometrico_id;
				$obj->codigo = $pcodigo;
				$obj->save();
			}
		}
		return $obj;
	}

	public function ItensDaLinha(){
		$qtd = DB::select("select count(*)
			from itens i 
			join itens_necessarios itn on i.id = itn.item_id
			join spools s on s.id = itn.spool_id
			where s.linha_id = ".$this->id."
			group by i.id, i.codigo, i.descricao
			");
		
		return $qtd;
		
	}
	
	public function SpoolsDaLinha(){
		$spools = Spool::where("linha_id", "=", $this->id);
		return $spools;
	}
	
}