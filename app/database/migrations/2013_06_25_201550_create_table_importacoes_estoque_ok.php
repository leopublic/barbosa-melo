<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateTableImportacoesEstoqueOk extends Migration {

    public function up()
    {
        Schema::create('importacoes_estoque', function(Blueprint $table) {
			$table->increments('id');
			$table->integer('usuario_id')->unsigned()->nullable();
			$table->string('nome_arquivo', 500);
			$table->datetime('data_importacao')->nullable();
			$table->integer('tipo')->unsigned()->nullable();
            $table->timestamps();
			$table->text('resultado')->nullable();
        });
    }

    public function down()
    {
        Schema::drop('importacoes_estoque');
    }

}
