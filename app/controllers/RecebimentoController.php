<?php

class RecebimentoController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function getIndex()
	{
		$recebimentos = Recebimento::
							with('ItemRecebido')
							->get();
		return View::make('recebimento')->with('recebimentos', $recebimentos);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		//
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}
	
	public function getImporte($id)
	{
		$caminho = "../public/uploads/rec/".$id;
		if (file_exists($caminho)) {
			$handle = fopen($caminho, "r");
			$linha = 1;
			$buffer = fgets($handle);
			$buffer = utf8_encode(fgets($handle));

			while (!feof($handle) || strlen($buffer) >10) {
				if($linha > 10 && trim(str_replace("\n", "", str_replace("\t", "", $buffer))) != ''){
					if(substr_count($buffer,"\t") < 10){
						while(substr_count($buffer,"\t") < 10){
							$buffer .= utf8_encode(fgets($handle));
							$linha++;
						}
					}
					$colunas = explode("\t", $buffer);
					$local = Local::Valido($colunas[15]);
					$armazem = Armazem::Valido($colunas[14], $local->id);
					$item = Item::Valido($colunas[8], $colunas[7], null, null);
					$ordem_compra = OrdemCompra::Valido(str_replace(",", ".", $colunas[0]));
					$recebimento = Recebimento::Valido(date('Y-m-d'), $ordem_compra->id, $armazem->id);
					$qtd = str_replace(",", ".",$colunas[13]);
					$recebimento->AdicionarItem($item->id, $qtd);
				}
				$buffer = utf8_encode(fgets($handle));
				$linha++;
			}
			fclose($handle);
		}
		else{
			print 'Arquivo '.$caminho.' não encontrado';
		}
	}
}