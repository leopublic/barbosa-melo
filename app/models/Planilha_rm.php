<?php
class Planilha_rm {

	protected $caminhoArquivo;
	protected $qtdLinhasCabecalho;

	/**
	 * Abre uma planilha para processamento
	 * @param type $pCaminhoArquivo
	 */
	public static function Nova($pCaminhoArquivo) {
		set_time_limit(0);
		$inputFileType = PHPExcel_IOFactory::identify($pCaminhoArquivo);
		$objReader = PHPExcel_IOFactory::createReader($inputFileType);
		
//		$objReader->setReadDataOnly(true);
		$chunkSize = 20;
		for ($startRow = 2; $startRow <= 240; $startRow += $chunkSize) {
			echo 'Loading WorkSheet using configurable filter for headings row 1 and for rows ',$startRow,' to ',($startRow+$chunkSize-1),'<br />';
			/**  Create a new Instance of our Read Filter, passing in the limits on which rows we want to read  **/
			$chunkFilter = new chunkReadFilter($startRow,$chunkSize);
			/**  Tell the Reader that we want to use the new Read Filter that we've just Instantiated  **/
			$objReader->setReadFilter($chunkFilter);
			
			/**  Load only the rows that match our filter from $inputFileName to a PHPExcel Object  **/
			$objPHPExcel = $objReader->load($pCaminhoArquivo);
			$sheetData = $objPHPExcel->getActiveSheet()->toArray(null,true,true,true);
			var_dump($sheetData);
		}
		while (1 == 0) {
			//Cadastra 
			$area = Area::where('descricao', '=', $area_val)->get();

			//Cadastra item
			//Cadastra rm
		}
	}
	
	public static function ImportarCsv($pCaminhoArquivo)
	{
		$handle = fopen($pCaminhoArquivo, "r");
		if ($handle) {
			$linha = 1;
			$buffer = fgets($handle);
			$buffer = utf8_encode(fgets($handle));
			$codigo_req = '';
			while (!feof($handle) || strlen($buffer) >10 ) {
//				if($linha % 50 == 0){
//					print "<br/>";
//				}
//				print $linha.'=';
				
				if(substr_count($buffer,"\t") < 40){
					while(substr_count($buffer,"\t") < 40){
						$buffer .= utf8_encode(fgets($handle));
						$linha++;
					}
				}
				$colunas = explode("\t", $buffer);
				$area = Area::Valido($colunas[4]);
				$familia_material = FamiliaMaterial::Valido($colunas[10]);
				$tipo_requisicao = TipoRequisicao::Valido($colunas[1]);
				$unidade = Unidade::Valido($colunas[9]);
				$item = Item::Valido($colunas[7], $colunas[6], $unidade->id, $familia_material->id );
				
				if($codigo_req != $colunas[0]){
					$codigo_req = $colunas[0];
					$data = explode("/", trim($colunas[2]));
					if(count($data) == 3){
						$data_fmt = '20'.$data[2].'-'.$data[1].'-'.$data[0];						
					}
					else{
						$data_fmt = '';
					}
					$requisicao = RequisicaoMaterial::Valido($colunas[0], $data_fmt, $tipo_requisicao->id, $area->id);					
				}
				
				$ind = self::UltimaRevisao($colunas, 52);
				$ind_qtd = $ind-22;
				$valor = $colunas[$ind_qtd];
				
				$requisicao->AdicionarItem($item, $colunas[5], $valor);
				
				$linha++;
				$buffer = utf8_encode(fgets($handle));
			}
			fclose($handle);
		}
		else{
			print "sem handle";
		}

	}
	
	public static function UltimaRevisao($pcolunas, $ind, $pprimeira = 33)
	{
		$val = trim(str_replace("\t", '', str_replace('"', '', $pcolunas[$ind])));
		if($val == ""){
			if($ind == $pprimeira){
				return $pprimeira;
			}
			else{
				return self::UltimaRevisao($pcolunas, $ind-1, $pprimeira);				
			}
		}
		else{
			return $ind;
		}
	}
}