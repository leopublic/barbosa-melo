<?php

use Illuminate\Database\Migrations\Migration;

class CreateTableAreas extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('areas', function($table)
		{
			$table->engine = 'InnoDB';
			$table->increments('id');
			$table->string('descricao', 300);
			$table->timestamps();
		});	
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('areas');
	}

}