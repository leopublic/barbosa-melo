@extends('layout')

@section('conteudo')
<div class="interno">
	{{ Form::open() }}
	<div style="float:right; padding-top:8px;">
		{{ Form::submit('Filtrar') }}
	</div>
	<h1>Controle de Isométricos</h1>
	<div class="grid filtro">
		<div class="row">
			<p class="slot-6">Código:</p><p class="slot-7">{{ Form::text('codigo', Input::old('codigo')) }}</p>
			<p class="slot-8">Status SIGEM:</p><p class="slot-9">{{ Form::select('status_sigem_id', array('' => '(selecione)') + $status_sigem, Input::old('status_sigem_id')) }}</p>
		</div>
		<div class="row">
			<p class="slot-6">Status Emissão:</p><p class="slot-7">{{ Form::select('status_emissao_id',  array('' => '(selecione)') + $status_emissao, Input::old('status_emissao_id')) }}</p>
		</div>
	</div>
	<table class="listagem">
		<colgroup>
			<col width="200px">
			<col width="40px">
			<col width="200px">
			<col width="200px">
			<col width="80px">
			<col width="auto">
		</colgroup>
		<thead>
			<tr>
				<th>Isométrico</th>
				<th class="c">Rev.</th>
				<th class="c">Status SIGEM</th>
				<th class="c">Status Emissão</th>
				<th class="r">Peso total</th>
				<th>Pendência</th>
			</tr>			
		</thead>
		<tbody>
		@if (isset($isometricos))
			@foreach ($isometricos as $iso)
			<tr>
				<td>{{ $iso->codigo }}</td>
				<td class="c">{{ $iso->revisao }}</td>
				<td class="c">{{ $iso->descricaoSigem }}</td>
				<td class="c">{{ $iso->descricaoEmissao }}</td>
				<td class="r">{{ number_format($iso->peso, 2, ",",".") }}</td>
				<td>{{ $iso->pendencia }}</td>
			</tr>
			@endforeach			
		@endif
		</tbody>
	</table>
	{{ Form::close() }}	
</div>
@stop