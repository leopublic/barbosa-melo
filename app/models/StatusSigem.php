<?php

class StatusSigem extends Eloquent {
	protected $table = 'status_sigem';
    protected $guarded = array();

    public static $rules = array();
	/**
	 * Cadastra um novo ou recupera se houver. 
	 * Utilizado nas importacoes
	 * @param string $pdescricao Descricao da area que se deseja cadastrar.
	 * @return UnidadeProd Retorna uma instancia de UnidadeProd
	 */
	public static function Valido($pdescricao)
	{
		if($pdescricao == ''){
			$obj = new StatusSigem();
		}
		else{
			$obj = StatusSigem::where('descricao', '=', $pdescricao)->first();
			if(!isset($obj)){
				$obj = new StatusSigem();
				$obj->descricao = $pdescricao;
				$obj->save();
			}
		}
		return $obj;
	}	
}