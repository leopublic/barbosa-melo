<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeyStatusIdToSpoolsTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
		Schema::table('spools', function($table)
		{
			$table->foreign('status_spool_id')->references('id')->on('status_spool');
		});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('spools', function(Blueprint $table) {
            $table->dropForeign('status_spool_id');
        });
    }

}
