<?php
/**
 * @property int $id Chave
 * @property int $tipo_requisicao_id Tipo de requisicao
 * @property int $area_id Area 
 * @property int $codigo Codigo da requisicao
 * @property date $dt_cadastro Data de cadastro
 * @property int $revisao Número da revisao
 * @property date $dt_revisao Data da revisao
 */
class RequisicaoMaterial extends Eloquent{
	protected $table = 'requisicoes_material';
	
	public function Item()
	{
		return $this->belongsToMany('Item', 'itens_solicitados', 'requisicao_material_id', 'item_id');
	}
	
	public function ItemSolicitado()
	{
		return $this->hasMany('ItemSolicitado');
	}
	
	/**
	 * Cadastra uma nova ou recupera se houver. 
	 * Utilizado nas importacoes
	 * @param string $pdescricao Descricao da classe que se deseja cadastrar.
	 * @return Area Retorna uma instancia da classe
	 */
	public static function Valido($pcodigo, $pdata_cadastro, $ptipo_requisicao_id, $parea_id)
	{
		$obj = RequisicaoMaterial::where('codigo', '=', $pcodigo)->first();
		if(!isset($obj)){
			$obj = new RequisicaoMaterial();
			$obj->codigo = $pcodigo;
			$obj->data_cadastro = $pdata_cadastro;
			$obj->tipo_requisicao_id = $ptipo_requisicao_id;
			$obj->area_id = $parea_id;
			$obj->save();
		}
		return $obj;
	}

	public function AdicionarItem($pitem, $pordem, $pqtd)
	{
		$id = DB::table('itens_solicitados')->insertGetId(array(
			'item_id' => $pitem->id,
			'requisicao_material_id'  => $this->id,
			'ordem'  => $pordem,
			'qtd'  => $pqtd
		));

	}
}