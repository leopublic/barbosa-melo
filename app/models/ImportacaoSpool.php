<?php
/**
 * @property int $usuario_id Usuario que fez a importação
 * @property string $nome_arquivo Nome do arquivo que foi importado
 * @property date $data_importacao Data em que o arquivo foi importado
 */
class ImportacaoSpool extends Importacao{
	protected $table = 'importacoes_spool';

	public static function get_subdiretorio()
	{
		return "spo";
	}
	
	/**
	 * TPSPOOL
	 */
	public function ImportarTipo1()
	{
		$plan = new Planilha();
		print $this->get_caminhoArquivo();
		$plan->abrir($this->get_caminhoArquivo(), 1, 22);
		while(!$plan->eof()){
			// Processa registro
			
			$iso = Isometrico::Valido(str_replace("\n", "", $plan->col(21)));
			$linha = Linha::Valido(str_replace('""', '"', $plan->col(5)), $iso->id);
			$codigo = $plan->col(0).'-'.substr('000'.$plan->col(1), -3);
			$spool = Spool::Valido($codigo, $linha->id);

			$spool->revisao			= $plan->col(4);
			$spool->espessura		= $plan->col(8);
			$spool->diametro_1		= $plan->col(7);
			$spool->peso			= $plan->col(9);
//			$spool->comprimento		= $plan->col(6);
			$spool->cond_pintura	= $plan->col(10);

			$spool->save();
			// Le próximo
			$plan->obterProximo();				
		}
		$this->data_importacao = date('Y-m-d H:i:s');
		$this->save();
	}
	/**
	 * MATSPOOL
	 */
	public function ImportarTipo2()
	{
		$plan = new Planilha();
		$plan->abrir($this->get_caminhoArquivo(), 1, 11);
		$i = 0;
		while(!$plan->eof()){
			$i++;
			// Processa registro
			$codigo = $plan->col(0).'-'.substr('000'.$plan->col(1), -3);
			$spool = Spool::where('codigo', '=', $codigo)->first();
			if(isset($spool)){
				$item = Item::ValidoResumido($plan->col(10), $plan->col(3));
				$unidade = Unidade::Valido($plan->col(6));
				$qtd = $plan->col(7);
				if($plan->col(6) == 'mm'){
					$qtd = $qtd / 1000;
				}
				$spool->AdicionarItemNecessario($item->id, $qtd, 'null', $unidade->id);				
			}
			// Le próximo
			$plan->obterProximo();
		}
		$this->data_importacao = date('Y-m-d H:i:s');
		$this->resultado = $i." registros processados";
		$this->save();
	}

	/**
	 * ControlTub
	 */
	public function ImportarTipo3()
	{
		// Importação da planilha do control tub
		set_time_limit(0);
		$plan = new Planilha();
		$plan->abrir($this->get_caminhoArquivo(), 12, 52);
		// Obtem indice das colunas
		$indCodUnidade = $plan->indiceColuna('UNIDADE');
		$indDescUnidade = $plan->indiceColuna('DESCRIÇÃO');
		$indIsometrico = $plan->indiceColuna('DESENHO PROJETO');
		$indLinha = $plan->indiceColuna('LINHA');
		$indSpool = $plan->indiceColuna('SPOOL');
		$indComprimento = $plan->indiceColuna('COMPRIMENTO');
		$indArea = $plan->indiceColuna('AREA');
		$indPeso = $plan->indiceColuna('PESO');
		$indRevisaoSei = $plan->indiceColuna('REVISAO');
		$indLiberacaoFab = $plan->indiceColuna('DATA LIBERAÇÃO FAB');
		$indCorte = $plan->indiceColuna('DATA CORTE');
		$indAjuste = $plan->indiceColuna('DATA AJUSTE');
		$indSolda = $plan->indiceColuna('DATA SOLDA');
		$indVisualSolda = $plan->indiceColuna('DATA VISUAL SOLDA');
		$indLiberacao = $plan->indiceColuna('DATA LIBERAÇÃO');
		$indInspetorDim = $plan->indiceColuna('ISNPETOR DIMENSIONAL');
		$indDimensional = $plan->indiceColuna('DATA DIMENSIONAL');
		$indPinturaFundo = $plan->indiceColuna('DATA PINTURA FUNDO');
		$indPinturaAcab = $plan->indiceColuna('DATA PINTURA ACABAMENTO');
		$indStatusControltub = $plan->indiceColuna('SITUAÇÃO SPOOL');

		$i_lidos = 0;
		$i_processados = 0;
		$peso_total = 0;
		$peso_ignorado = 0;
		while(!$plan->eof()){
			// Processa registro
			$i_lidos++;
			$peso= str_replace(',', '.',str_replace('.', '',$plan->col($indPeso)));
			$peso_total =+ $peso;
			if($plan->col($indIsometrico) != ''){
				$i_processados++;
				//Atualiza unidade
				$unidade = UnidadeProd::Valido($plan->col($indCodUnidade), $plan->col($indDescUnidade));
				if($unidade->descricao == ''){
					$unidade->descricao = $plan->col($indDescUnidade);
					$unidade->save();
				}
				$isometrico = Isometrico::Valido($plan->col($indIsometrico));
				$isometrico->ok_controltub = 1;
				$isometrico->save();

				$linha = Linha::Valido($plan->col($indLinha), $isometrico->id);
				$spool = Spool::Valido($plan->col($indSpool), $linha->id);

				$statusControltub = StatusControltub::Valido($plan->col($indStatusControltub));
				$spool->status_controltub_id = $statusControltub->id;

				$spool->comprimento			= $plan->col($indComprimento);
				$spool->area				= $plan->col($indArea);
				$spool->peso				= str_replace(',', '.',str_replace('.', '',$plan->col($indPeso)));
				$spool->revisao_sei			= $plan->colDt($indRevisaoSei, 'd/m/y', 'Y-m-d');
				$spool->liberacao_fab		= $plan->colDt($indLiberacaoFab, 'd/m/y', 'Y-m-d');
				$spool->corte				= $plan->colDt($indCorte, 'd/m/y', 'Y-m-d');
				$spool->ajuste				= $plan->colDt($indAjuste, 'd/m/y', 'Y-m-d');
				$spool->solda				= $plan->colDt($indSolda, 'd/m/y', 'Y-m-d');
				$spool->visual_solda		= $plan->colDt($indVisualSolda, 'd/m/y', 'Y-m-d');
				$spool->liberacao			= $plan->colDt($indLiberacao, 'd/m/y', 'Y-m-d');
				$spool->inspetor_dim		= $plan->col($indInspetorDim);
				$spool->dimensional			= $plan->colDt($indDimensional, 'd/m/y', 'Y-m-d');
				$spool->pintura_fundo		= $plan->colDt($indPinturaFundo, 'd/m/y', 'Y-m-d');
				$spool->pintura_acabamento	= $plan->colDt($indPinturaAcab, 'd/m/y', 'Y-m-d');

				// Precisa melhorar essa lógica.
				if($statusControltub->descricao == '35 - LIBERADO PARA MONTAGEM'){
					$spool->status_spool_id = 5;	// Pintado ou liberado para montagem....
				}
				else{
					if(( $spool->visual_solda != '' && $spool->visual_solda != '0000-00-00')
					|| ( $spool->dimensional != '' && $spool->dimensional != '0000-00-00')){
						$spool->status_spool_id = 3;
					}
					else{
						if( ($spool->corte  != '' && $spool->corte != '0000-00-00')
						 || ($spool->ajuste  != '' && $spool->ajuste != '0000-00-00')
						 || ($spool->solda  != '' && $spool->solda != '0000-00-00')){
							$spool->status_spool_id = 2;
						}
						else{
							if($spool->status_spool_id != 7){
								if($isometrico->status_emissao_id == 1 
								&& $isometrico->pendencia == '' 
								&& $isometrico->col_q == '0' 
								&& $isometrico->col_r == ''){
									$spool->status_spool_id = 1;
								}
								else{
									$spool->status_spool_id = 6;	// não liberado							
								}							
							}
						}
					}
				}

				$spool->ok_controltub = 1;
				$spool->save();
			}
				// Le próximo
			$plan->obterProximo();
		}
		$this->data_importacao = date('Y-m-d H:i:s');
		$this->resultado = $i_lidos." registros lidos<br/>".$i_processados." registros processados";
		$this->save();
	}
	
	public function ImportarTPSPOOLExcel(){
		set_time_limit(0);
		$i = 0;
		$inputFileType = PHPExcel_IOFactory::identify($this->get_caminhoArquivo());
		$objReader = PHPExcel_IOFactory::createReader($inputFileType);

		$objReader->setReadDataOnly(true);
		$objPHPExcel = $objReader->load($this->get_caminhoArquivo());
		$sheetData = $objPHPExcel->getActiveSheet()->toArray(null,true,true,true);
		if(count($sheetData[1]) < 22){
			$this->resultado = "Importação não realizada, pois o arquivo está fora do padrão: menos de 22 colunas.";
		}
		else{
			for ($index = 2; $index <= count($sheetData) ; $index++) {
				$i++;
				$l = $sheetData[$index];
				$iso = Isometrico::Valido(str_replace("\n", "", $l['V']));
				$iso->ok_tpspool = 1;
				$iso->save();

				$linha = Linha::Valido(str_replace('""', '"', $l['F']), $iso->id);
				$codigo = $l['A'].'-'.substr('000'.str_replace('#', '', $l['B']), -3);
				$spool = Spool::Valido($codigo, $linha->id);

				$spool->revisao			= $l['E'];
				$spool->espessura		= $l['I'];
				$spool->diametro_1		= $l['H'];
				// $spool->peso			= $l['J'];
	//			$spool->comprimento		= $plan->col(6);
				$spool->cond_pintura	= $l['K'];

				$spool->ok_tpspool = 1;
				$spool->save();
				// Le próximo
			}			
			$this->resultado = $i." registros processados";				
		}
		$this->data_importacao = date('Y-m-d H:i:s');
		$this->save();
	}
	
	public function ImportarMATSPOOLExcel(){
		set_time_limit(0);
		$i = 0;
		$msg = "";
		try{
			$inputFileType = PHPExcel_IOFactory::identify($this->get_caminhoArquivo());
			$objReader = PHPExcel_IOFactory::createReader($inputFileType);

			$objReader->setReadDataOnly(true);
			$objPHPExcel = $objReader->load($this->get_caminhoArquivo());
			$sheetData = $objPHPExcel->getActiveSheet()->toArray(null,true,true,true);
			for ($index = 2; $index <= count($sheetData) ; $index++) {
				$i++;
				$l = $sheetData[$index];

				$codigo = $l['A'].'-'.substr('000'.str_replace('#', '', $l['B']), -3);
				$spool = Spool::where('codigo', '=', $codigo)->first();
				if(isset($spool)){
					$spool->ok_matspool = 1;
					$spool->save();

					$item = Item::ValidoResumido($l['K'], $l['C']);
					$unidade = Unidade::Valido($l['G']);
					$qtd = $l['H'];
					if($l['G'] == 'mm'){
						$qtd = $qtd / 1000;
					}
					$spool->AdicionarItemNecessario($item->id, $qtd, 'null', $unidade->id);				
				}
				else{
					$msg = 'Encontrados itens para o spool '.$codigo.' mas o spool nao estava cadastrado.';
				}
			}
			$msg .= $i." registros processados";
		}
		catch(\Exception $e){
			$msg .= "Importação interrompida no registro ".$i." por erro: <br/>".$e->getMessage();
		}
		$this->resultado = $msg;
		$this->data_importacao = date('Y-m-d H:i:s');
		$this->save();
	}
}