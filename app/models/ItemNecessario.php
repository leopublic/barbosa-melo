<?php
/**
 * @property int $id Chave
 * @property integer $spool_id Chave do spool
 * @property integer $item_id Chave do item
 * @property integer $unidade_id Chave da unidade (não obrigatorio)
 * @property decimal $qtd Quantidade requisitada
 * @property decimal $peso Peso requisitado
 */
class ItemNecessario extends Eloquent{
	protected $table = 'itens_necessarios';
	
	public function item(){
		return $this->belongsTo('Item');
	}
	
	public function spool(){
		return $this->belongsTo('Spool');
	}
	
}