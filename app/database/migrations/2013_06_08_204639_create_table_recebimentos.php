<?php

use Illuminate\Database\Migrations\Migration;

class CreateTableRecebimentos extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('recebimentos', function($table)
		{
			$table->engine = 'InnoDB';
			$table->increments('id');
			$table->integer('ordem_compra_id')->unsigned()->nullable();
			$table->integer('armazem_id')->unsigned()->nullable();
			$table->string('data_recebimento');
			$table->string('descricao',500);
			$table->timestamps();
		});			

		Schema::table('recebimentos', function($table)
		{
			$table->foreign('ordem_compra_id')->references('id')->on('ordens_compra');
			$table->foreign('armazem_id')->references('id')->on('armazens');
		});
	}
		

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('recebimentos');
	}

}