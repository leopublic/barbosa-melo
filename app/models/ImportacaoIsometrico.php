<?php
/**
 * @property int $usuario_id Usuario que fez a importação
 * @property string $nome_arquivo Nome do arquivo que foi importado
 * @property date $data_importacao Data em que o arquivo foi importado
 */
class ImportacaoIsometrico extends Importacao{
	protected $table = 'importacoes_isometrico';

	public static function get_subdiretorio()
	{
		return "iso";
	}
	
	public function Importe()
	{
		$time_start = microtime(true);

		$plan = new Planilha();
		$plan->abrir($this->get_caminhoArquivo(), 4, 17);
	
		$qtd = 0;
		$qtd_novos = 0;
		$qtd_atualizados = 0;
		$peso_total = 0;
		$peso_ignorado = 0;
		$peso_gravado = 0;
		while(!$plan->eof()){
			// Processa registro
			//Obtem ou cria o isometrico pelo codigo
			$peso = str_replace(",", ".", str_replace(".", "", $plan->col(10)));

			$peso_total += $peso;
			$qtd++;
			$codigo = $plan->col(1);
			if($codigo != 'unset'){
				$codigo = Isometrico::CodigoPadrao($codigo);
				$iso = Isometrico::where('codigo', '=', $codigo)->first();
				if(!isset($iso)){
					$iso = new Isometrico();
					$iso->codigo = $codigo;
					$iso->save();
					$qtd_novos++;
				}
				else{
					$qtd_atualizados++;
				}
				//Obtem os atributos que devem ser atualizados
				$unidade_prod = UnidadeProd::Valido($plan->col(0));

				$cod_linha = str_replace('""', '"', substr($plan->col(5),1));		// Retirando a barra na frente e as aspas duplas
				$linha = Linha::Valido($cod_linha, $iso->id);

				$desc = substr($plan->col(3), 0, -2);

				$status_sigem = StatusSigem::Valido($desc);
				$status_emissao = StatusEmissao::Valido($plan->col(6));
				$grd = $plan->col(7);
				$peso = str_replace(",", ".", str_replace(".", "", $plan->col(10)));
	//			$dt_inclusao = $plan->colDt(4, 'd/m/Y H:i:s');
	//			$dt_emissao = $plan->colDt(8, 'Y/m/d');
				$pend = '';
				$br = '';
				if(trim($plan->col(11)) != ''){
					$pend .= $br.trim($plan->col(11));
					$br = "<br/>";
				}
				if(trim($plan->col(12)) != ''){
					$pend .= $br.trim($plan->col(12));
					$br = "<br/>";
				}
				if(trim($plan->col(13)) != ''){
					$pend .= $br.trim($plan->col(13));
					$br = "<br/>";
				}
				if(trim($plan->col(14)) != ''){
					$pend .= $br.trim($plan->col(14));
					$br = "<br/>";
				}

				$iso->col_q = $plan->col(16);
				$col_r = $plan->col(17);
				if($col_r != ''){
					$iso->col_r = $col_r;
				}
				else{
					$iso->col_r = null;
				}

				$revisao = substr($plan->col(2),0,1);

				$iso->unidade_prod_id = $unidade_prod->id;
				$iso->status_sigem_id = $status_sigem->id;
				$iso->status_emissao_id = $status_emissao->id;
				$iso->grd = $grd;
				if($peso != '' && intval($iso->peso) == 0){
					$iso->peso = $peso;					
				}
				$iso->revisao = $revisao;
				$iso->pendencia = $pend;
				$iso->data_inclusao_controle = $plan->colDt(4, 'd/m/Y H:i:s');
				// $iso->data_emissao =  $plan->colDt(8, 'Y/m/d');
				$iso->ok_sei = 1;
				$iso->save();
				$peso_gravado += $peso;
			}
			else{
				$peso_ignorado += $peso;
			}

			// Le próximo
			$plan->obterProximo();
		}
		$time_end = microtime(true);
		$time = $time_end - $time_start;
		$resultado = $qtd." linhas lidas.<br/>".$qtd_novos." novos isométricos identificados.<br/>".$qtd_atualizados." isométricos atualizados.";
		$resultado = "<br/>Peso total ".$peso_total."<br/>peso ignorado ".$peso_ignorado."<br/>peso gravado ".$peso_gravado;
		$resultado .= "<br/>Processamento concluído em ".$time." segundos.";
		$this->data_importacao = date('Y-m-d H:i:s');
		$this->resultado = $resultado;
		$this->save();
	}
}