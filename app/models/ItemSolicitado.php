<?php
class ItemSolicitado extends Eloquent{
	protected $table = 'itens_solicitados';
	
	public function item(){
		return $this->belongsTo('Item');
	}
	
}