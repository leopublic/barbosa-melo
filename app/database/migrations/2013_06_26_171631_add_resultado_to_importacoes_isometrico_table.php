<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddResultadoToImportacoesIsometricoTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('importacoes_isometrico', function(Blueprint $table) {
            $table->text('resultado')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('importacoes_isometrico', function(Blueprint $table) {
            $table->dropColumn('resultado');
        });
    }

}
