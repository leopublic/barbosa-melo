<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignUnidadeProdIdToIsometricos extends Migration {

    public function up()
    {
        Schema::table('isometricos', function(Blueprint $table) {
			$table->foreign('unidade_prod_id')->references('id')->on('unidades_prod');
        });
    }

    public function down()
    {
        Schema::table('isometricos', function(Blueprint $table) {
			$table->dropForeign('isometricos_unidade_prod_id_foreign');            
        });
    }

}
