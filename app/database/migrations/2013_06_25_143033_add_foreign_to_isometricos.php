<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignToIsometricos extends Migration {

    public function up()
    {
        Schema::table('isometricos', function(Blueprint $table) {
			$table->foreign('status_sigem_id')->references('id')->on('status_sigem');
			$table->foreign('status_emissao_id')->references('id')->on('status_emissao');
        });
    }

    public function down()
    {
        Schema::table('isometricos', function(Blueprint $table) {
			$table->dropForeign('isometricos_status_sigem_id_foreign');
			$table->dropForeign('isometricos_status_emissao_id_foreign');
        });
    }

}
