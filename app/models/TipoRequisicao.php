<?php
/**
 * @property int $id Chave
 * @property string $descricao Descricao
 */
class TipoRequisicao extends Eloquent{
	protected $table = 'tipos_requisicao';
	/**
	 * Cadastra uma nova ou recupera se houver. 
	 * Utilizado nas importacoes
	 * @param string $pdescricao Descricao da classe que se deseja cadastrar.
	 * @return Area Retorna uma instancia da classe
	 */
	public static function Valido($pdescricao)
	{
		$obj = TipoRequisicao::where('descricao', '=', $pdescricao)->first();
		if(!isset($obj)){
			$obj = new TipoRequisicao();
			$obj->descricao = $pdescricao;
			$obj->save();
		}
		return $obj;
	}

}