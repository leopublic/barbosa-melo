<?php
/**
 * @property int $id Chave
 * @property string $descricao Descricao
 */
class Espec extends Eloquent{
	protected $table = 'especs';
	/**
	 * Cadastra um novo ou recupera se houver. 
	 * Utilizado nas importacoes
	 * @param string $pdescricao Descricao da area que se deseja cadastrar.
	 * @return Espec Retorna uma instancia de Espec
	 */
	public static function Valido($pdescricao)
	{
		if($pdescricao == ''){
			$obj = new Espec();
		}
		else{
			$obj = Espec::where('descricao', '=', $pdescricao)->first();
			if(!isset($obj)){
				$obj = new Espec();
				$obj->descricao = $pdescricao;
				$obj->save();
			}
		}
		return $obj;
	}	
}