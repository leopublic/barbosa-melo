<?php

use Illuminate\Database\Migrations\Migration;

class CreateTableTiposRequisicao extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('tipos_requisicao', function($table)
		{
			$table->engine = 'InnoDB';
			$table->increments('id');
			$table->string('sigla');
			$table->string('descricao', 300);
			$table->timestamps();
		});	
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('tipos_requisicao');
	}

}