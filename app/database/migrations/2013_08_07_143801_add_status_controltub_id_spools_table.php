<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddStatusControltubIdSpoolsTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('spools', function(Blueprint $table) {
            $table->integer('status_controltub_id')->unsigned()->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('spools', function(Blueprint $table) {
            $table->dropColumn('status_controltub_id');            
        });
    }

}
