<?php

use Illuminate\Database\Migrations\Migration;

class CreateTableOrdensCompra extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('ordens_compra', function($table)
		{
			$table->engine = 'InnoDB';
			$table->increments('id');
			$table->integer('fornecedor_id')->unsigned()->nullable();
			$table->string('numero');
			$table->date('data_emissao');
			$table->date('data_base')->nullable();
			$table->timestamps();
		});	
		
		Schema::table('ordens_compra', function($table)
		{
			$table->foreign('fornecedor_id')->references('id')->on('fornecedores');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('ordens_compra');
	}

}