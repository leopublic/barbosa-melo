<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <title>Barbosa Melo Engenharia</title>
	{{ HTML::style("css/style.css") }}
	{{ HTML::style("css/style_mini.css", array("media" => "screen and (max-width: 720px)")  ) }}
	{{ HTML::style("css/font-awesome/css/font-awesome.css") }}

	{{ HTML::style("css/720_grid.css", array("media" => "screen and (min-width: 720px)")  ) }}
	{{ HTML::style("css/986_grid.css", array("media" => "screen and (min-width: 986px)")  ) }}
	{{ HTML::style("css/1236_grid.css", array("media" => "screen and (min-width: 1236px)")  ) }}

	{{ HTML::style("css/footable-0.1.css") }}

	{{ HTML::script('js/jquery-1.9.1.js') }}
	{{ HTML::script('http://code.jquery.com/jquery-migrate-1.2.1.js') }}
	{{ HTML::script('js/script.js') }}
	<!--{{ HTML::script('js/noty/jquery.noty.js') }}-->
	{{ HTML::script('js/jquery.floatheader.min.js') }}
	<script>
		$(document).ready(function(){
			// $('table.listagem').find('> tbody > tr:nth-child(even)').addClass('even');
			// $('table.listagem').find('> tbody > tr > td.iso:even').addClass('red');
		});
		
//		$.noty.closeAll();
		$(document).ready(function(){
			$('.listagem').floatHeader();					// Ativa o cabecalho fixo
		});
	</script>
</head>
<body>
	<div id="barra">
		<h1><img src="../img/logo.png"/></h1>
		@if (Auth::check())
		<div class="xmenu">
			<ul>
				<li>Importações
					<ul>
						<li><a href="{{ URL::to('importacaorm') }}">Requisições de compra</a></li>
						<li><a href="{{ URL::to('importacaooc') }}">Ordens de compra</a></li>
						<li><a href="{{ URL::to('importacaorec') }}">Recebimentos</a></li>
						<li><a href="{{ URL::to('importacaoiso') }}">Isométricos</a></li>
						<li><a href="{{ URL::to('importacaospool') }}">Spools</a></li>
						<li><a href="{{ URL::to('importacaoestoque') }}">Estoque</a></li>
					</ul>
				</li>
				<li>Suprimentos
					<ul>
						<li><a href="{{ URL::to('rm/index') }}">Requisições</a></li>
						<li><a href="{{ URL::to('ordemcompra/index') }}">Ordens de compra</a></li>
						<li><a href="{{ URL::to('recebimento/index') }}">Recebimentos</a></li>
					</ul>
				</li>
				<li>Materiais
					<ul>
						<li><a href="{{ URL::to('materiais') }}">Saldo</a></li>
						<li><a href="{{ URL::to('materiais/historico') }}">Histórico</a></li>
					</ul>
				</li>
				<li>Planejamento & Produção
					<ul>
						<li><a href="{{ URL::to('isometrico') }}">Controle de Isométricos</a></li>
						<!-- <li><a href="{{ URL::to('isometrico/materiais') }}">Isométricos x Materiais</a></li> -->
						<li><a href="{{ URL::to('spool/index') }}">Controle de Spools</a></li>
						<li><a href="{{ URL::to('spool/verificacao') }}">Conferência das importações</a></li>
						<li><a href="{{ URL::to('spool/materiais') }}">Programação de Fabricação de Spools</a></li>
<!-- 						<li><a href="{{ URL::to('spool/fabricadossemdesenho') }}">Spools fabricados fora da SEI</a></li>
						<li><a href="{{ URL::to('spool/desenhadosfabricados') }}">Spools da SEI fabricados</a></li>
 -->					</ul>
				</li>
				<li><a href="{{ URL::to('logout') }}">Sair</a></li>
			</ul>
		</div>
		@endif
	</div>
	<div id="conteudo">
		@yield('conteudo')
    <div>
	
</body>
</html>
