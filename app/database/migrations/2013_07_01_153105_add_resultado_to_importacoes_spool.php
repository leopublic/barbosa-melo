<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddResultadoToImportacoesSpool extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('importacoes_spool', function(Blueprint $table) {
			$table->text('resultado')->nullable();            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('spool', function(Blueprint $table) {
            $table->dropColumn('resultado');
        });
    }

}
