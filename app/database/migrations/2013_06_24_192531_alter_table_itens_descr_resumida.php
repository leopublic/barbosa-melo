<?php
use Illuminate\Database\Migrations\Migration;
class AlterTableItensDescrResumida extends Migration {
	public function up()
	{
		Schema::table('itens', function($table)
		{
			$table->string('descricao_resumida', 2000);
		});
	}

	public function down()
	{
		Schema::table('itens', function($table)
		{
			$table->dropColumn('descricao_resumida');
		});
	}

}