<?php

use Illuminate\Database\Migrations\Migration;

class AlterTableItensNecessarios2 extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('itens_necessarios', function($table)
		{
			$table->integer('unidade_id')->unsigned()->nullable();
		});
		Schema::table('itens_necessarios', function($table)
		{
			$table->foreign('unidade_id')->references('id')->on('unidades');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
	}

}