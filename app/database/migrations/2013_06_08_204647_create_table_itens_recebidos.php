<?php

use Illuminate\Database\Migrations\Migration;

class CreateTableItensRecebidos extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('itens_recebidos', function($table)
		{
			$table->engine = 'InnoDB';
			$table->increments('id');
			$table->string('descricao',500);
			$table->timestamps();
		});			
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('itens_recebidos');
	}

}