@extends('layout')

@section('conteudo')
<div class="interno">
	<div id="header">
		<div class="acoes">
		</div>
		<h1>Importações controle geral de isométricos</h1>
	</div>
	<div class="form">
	{{ Form::open(array('url' => 'importacaoiso', 'files' => true)) }}
		<ul>
			<li>
				<p class="label">Informe o arquivo a ser importado:</p>
				<p class="content">{{ Form::file('arquivo',array('id'=> '0')); }}</p>
			</li>
		</ul>
	<div style="text-align: center;margin: auto;">
		{{ Form::submit('Enviar'); }}
	</div>
	@if (Session::has('msg'))
		<span class="error">{{ Session::get('msg') }}</span>
	@endif	
	{{ Form::close() }}	
	</div>
	<table class="listagem">
		<thead>
			<tr>
				<th class="c">Ação</th>
				<th>Arquivo</th>
				<th>Carregado em</th>
				<th>Importado em</th>
				<th>Observações</th>
			</tr>
		</thead>
		<tbody>
			@foreach ($importacoes as $importacao)
			<tr>
				<td class="c"><a href="importacaoiso/importe/{{ $importacao->id }}"><i class="icon-magic"></i></a> </td>
				<td>
					@if ($importacao->get_ArquivoExiste())
					<a href="/importacaoiso/arquivo/{{ $importacao->id }}" target="_blank" title="Clique para obter esse arquivo">{{ $importacao->nome_arquivo }}</a>
					@else
					<span style="text-decoration: line-through; ">{{ $importacao->nome_arquivo }}</span>
					@endif					
				</td>
				<td>{{ $importacao->created_at }}</td>
				<td>{{ $importacao->data_importacao }}</td>
				<td>{{ $importacao->resultado }}</td>
			</tr>
			@endforeach
		</tbody>
	</table>
	</div>
</div>
@stop