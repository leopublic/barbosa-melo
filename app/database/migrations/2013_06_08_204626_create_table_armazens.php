<?php

use Illuminate\Database\Migrations\Migration;

class CreateTableArmazens extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('armazens', function($table)
		{
			$table->engine = 'InnoDB';
			$table->increments('id');
			$table->string('descricao',500);
			$table->timestamps();
		});			
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('armazens');
	}

}