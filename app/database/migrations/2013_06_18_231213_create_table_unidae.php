<?php
use Illuminate\Database\Migrations\Migration;
class CreateTableUnidae extends Migration {
	public function up()
	{
		Schema::create('unidades_prod', function($table) {
			$table->increments('id');
			$table->string('descricao');
			$table->timestamps();
		});
	}

	public function down()
	{
		Schema::drop('unidades_prod');
	}
}