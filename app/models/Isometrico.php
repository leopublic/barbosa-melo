<?php
/**
 * @property int $id Chave
 * @property string $descricao Descricao
 * @property int $unidade_prod_id Unidade de producao do isometrico
 * @property int $status_sigem_id Status Sigem
 * @property int $status_emissao_id Status da emissao
 * @property string $revisao Ultima revisao
 * @property date $data_inclusao_controle Data de inclusao no controle
 * @property decimal $peso Peso total do isometrico pelo controle
 * @property string $grd Grd
 * @property date $data_emissao Data de emissao
 */
class Isometrico extends Eloquent{
	protected $table = 'isometricos';

	public function statussigem()
	{
		return $this->belongsTo('StatusSigem', 'status_sigem_id');
	}

	public function statusemissao()
	{
		return $this->belongsTo('StatusEmissao', 'status_emissao_id');
	}
	
	public function linha()
	{
		return $this->hasMany('Linha', 'isometrico_id');
	}

	public function qtdItens()
	{
		
	}
	
	/**
	 * Cadastra uma nova ou recupera se houver. 
	 * Utilizado nas importacoes
	 * @param string $pdescricao Descricao da area que se deseja cadastrar.
	 * @return Area Retorna uma instancia de Area
	 */
	public static function Valido($pcodigo)
	{
		$pcodigo = self::CodigoPadrao($pcodigo);
		if($pcodigo == ''){
			$obj = new Isometrico();
		}
		elseif(strlen($pcodigo) < 25){
			$obj = new Isometrico();			
		}
		else{
			$obj = Isometrico::where('codigo', '=', $pcodigo)->first();
			if(!isset($obj)){
				$obj = new Isometrico();
				$obj->codigo = $pcodigo;
				$obj->save();
			}			
		}
		return $obj;
	}

	public function ItensDoIsometrico(){
//		$itens = DB::table('itens_necessarios')
//                ->select(DB::raw('count(*) as user_count, status'))
//				->join('spools', 'spools.id', '=', 'itens_necessarios.spool_id' )
//				->join('linhas', 'linhas.id', '=', 'spools.linha_id' )
//				->where('linhas.isometrico_id', '=', $this->id)
//				->get();

		$itens = DB::select("select 
				  i.id, i.codigo codigo_item
				, i.descricao, sum(itn.qtd) qtd
				, (select sum(qtd) from itens_solicitados where item_id = i.id) qtd_solicitada
				, (select sum(qtd) from itens_comprados where item_id = i.id) qtd_comprada
				, (select sum(qtd) from itens_recebidos where item_id = i.id) qtd_recebida
			from itens i 
			join itens_necessarios itn on i.id = itn.item_id
			join spools s on s.id = itn.spool_id
			join linhas l on l.id = s.linha_id
			join isometricos iso on iso.id = l.isometrico_id
			where iso.id = ".$this->id."
			group by i.id, i.codigo, i.descricao
			");
		
		return $itens;
	}

	public function LinhasDoIsometrico(){
		$linhas = Linha::where("isometrico_id", "=", $this->id);
		return $linhas;
	}
	
	public static function CodigoPadrao($pcodigo)
	{
		if(substr($pcodigo,0,2) == 'IS'){
			$pcodigo = 'IC'.substr($pcodigo,2);
		}
		return $pcodigo;
	}
}