<?php

use Illuminate\Database\Migrations\Migration;

class CreateTableImportacoesRm extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('importacoes_rm', function($table)
		{
			$table->engine = 'InnoDB';
			$table->increments('id');
			$table->integer('usuario_id')->unsigned()->nullable();
			$table->string('nome_arquivo');
			$table->timestamps();
		});	
		
		Schema::table('importacoes_rm', function($table)
		{
			$table->foreign('usuario_id')->references('id')->on('usuarios');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('importacoes_rm');
	}
}