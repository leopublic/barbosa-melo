<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddControlToIsometricosTabl extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('spools', function(Blueprint $table) {
            $table->dropColumn('comprimento');
            $table->decimal('area', 10, 3)->nullable();
            $table->integer('total_junta')->nullable();
            $table->integer('total_junta_pipe')->nullable();
            $table->integer('total_junta_pipe_ajustada')->nullable();
            $table->integer('total_junta_pipe_soldada')->nullable();
            $table->integer('total_junta_pipe_aprovada')->nullable();
            $table->integer('total_junta_pipe_')->nullable();
            $table->string('revisao_sei', 3)->nullable();
            $table->date('liberacao_fab')->nullable();
            $table->date('corte')->nullable();
            $table->date('ajuste')->nullable();
            $table->date('solda')->nullable();
            $table->date('visual_solda')->nullable();
            $table->date('liberacao')->nullable();
            $table->string('inspetor_dim')->nullable();
            $table->date('dimensional')->nullable();
            $table->string('relatorio_dim')->nullable();
            $table->date('pintura_fundo')->nullable();
            $table->date('pintura_acabamento')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('spools', function(Blueprint $table) {
            $table->dropColumn('revisao_sei');
        });
    }

}
