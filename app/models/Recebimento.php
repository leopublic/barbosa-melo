<?php
/**
 * @property int $id Chave
 * @property date $data_recebimento Data do recebimento
 * @property int $armazem_id Armazem onde o recebimento foi feito
 * @property int $ordem_compra_id Ordem de compra que está sendo recebida
 */
class Recebimento extends Eloquent{
	protected $table = 'recebimentos';

	public function Armazem()
	{
		return $this->belongsTo('Armazem');
	}
	public function OrdemCompra()
	{
		return $this->belongsTo('OrdemCompra');
	}
	public function ItemRecebido()
	{
		return $this->hasMany('ItemRecebido');
	}
	/**
	 * Cadastra uma nova ou recupera se houver. 
	 * Utilizado nas importacoes
	 * @param string $pdescricao Descricao da area que se deseja cadastrar.
	 * @return Area Retorna uma instancia de Area
	 */
	public static function Valido($pdata_recebimento, $pordem_compra_id, $parmazem_id)
	{
		$obj = Recebimento::where('data_recebimento', '=', $pdata_recebimento)
				->where('ordem_compra_id', '=', $pordem_compra_id)
				->where('armazem_id', '=', $parmazem_id)
				->first();
		if(!isset($obj)){
			$obj = new Recebimento();
			$obj->data_recebimento = $pdata_recebimento;
			$obj->ordem_compra_id = $pordem_compra_id;
			$obj->armazem_id = $parmazem_id;
			$obj->save();
		}
		return $obj;
	}
	
	public function AdicionarItem($pitem_id, $pqtd)
	{
		DB::table('itens_recebidos')->insert(array(
			'item_id'			=> $pitem_id,
			'recebimento_id'	=> $this->id,
			'qtd'				=> $pqtd
		));
		
	}
}