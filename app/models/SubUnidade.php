<?php
/**
 * @property int $id Chave
 * @property string $descricao Descricao
 */
class SubUnidade extends Eloquent{
	protected $table = 'sub_unidades';
	/**
	 * Cadastra um novo ou recupera se houver. 
	 * Utilizado nas importacoes
	 * @param string $pdescricao Descricao da area que se deseja cadastrar.
	 * @return SubUnidade Retorna uma instancia de SubUnidade
	 */
	public static function Valido($pdescricao)
	{
		if($pdescricao == ''){
			$obj = new SubUnidade();
		}
		else{
			$obj = SubUnidade::where('descricao', '=', $pdescricao)->first();
			if(!isset($obj)){
				$obj = new SubUnidade();
				$obj->descricao = $pdescricao;
				$obj->save();
			}
		}
		return $obj;
	}	
}