<?php
use Illuminate\Database\Migrations\Migration;
class CreateTableFluido extends Migration {
	public function up()
	{
		Schema::create('fluidos', function($table) {
			$table->increments('id');
			$table->string('descricao');
			$table->timestamps();
		});
	}

	public function down()
	{
		Schema::drop('fluidos');
	}
}