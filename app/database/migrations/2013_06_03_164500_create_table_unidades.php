<?php

use Illuminate\Database\Migrations\Migration;

class CreateTableUnidades extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('unidades', function($table)
		{
			$table->engine = 'InnoDB';
			$table->increments('id');
			$table->string('sigla');
			$table->string('descricao');
			$table->timestamps();
		});	
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('unidades');
	}

}