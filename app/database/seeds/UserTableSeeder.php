<?php
class UserTableSeeder extends Seeder {

    public function run()
    {
        DB::table('users')->delete();

        User::create(array(
            'username' => 'leonardo',
            'password' => Hash::make('magrico')
        ));
        User::create(array(
            'username' => 'roberto',
            'password' => Hash::make('barbosa')
        ));
        User::create(array(
            'username' => 'anderson',
            'password' => Hash::make('barbosa')
        ));
        User::create(array(
            'username' => 'gealbes',
            'password' => Hash::make('barbosa')
        ));
        User::create(array(
            'username' => 'gebson',
            'password' => Hash::make('barbosa')
        ));
        User::create(array(
            'username' => 'russel',
            'password' => Hash::make('barbosa')
        ));
        User::create(array(
            'username' => 'renato',
            'password' => Hash::make('barbosa')
        ));
        User::create(array(
            'username' => 'danielle',
            'password' => Hash::make('barbosa')
        ));
        User::create(array(
            'username' => 'vinicius',
            'password' => Hash::make('barbosa')
        ));
    }
}