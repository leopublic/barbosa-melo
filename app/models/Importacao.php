<?php
class Importacao extends Eloquent{
	public static function get_subdiretorio()
	{
		return "";
	}
	
	public static function get_pasta()
	{
		return '..'. DIRECTORY_SEPARATOR . 'public'. DIRECTORY_SEPARATOR .'uploads'. DIRECTORY_SEPARATOR .static::get_subdiretorio();
	}

	public function get_caminhoArquivo()
	{
		if(!is_dir(static::get_pasta())){
			mkdir(static::get_pasta(), 0755, true);
		}
		return static::get_pasta() . DIRECTORY_SEPARATOR . $this->id;
	}

	public function get_ArquivoExiste()
	{
		if(file_exists($this->get_caminhoArquivo())){
			return true;
		}
		else{
			return false;
		}
	}
	
}
