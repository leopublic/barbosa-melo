<?php
use Illuminate\Database\Query;
use Illuminate\Database\Query\Builder;

class SpoolController extends \BaseController {
	public function getIndex()
	{
		$spools = Spool::all();
		return View::make('spools')->with('spools', $spools);
	}
	
	public function postIndex()
	{
		$spools = Spool::all();	
		return View::make('spools')->with('spools', $spools);
	}
	
	public function getVerificacao()
	{
		$sql = "select spo.id, iso.codigo codigo_isometrico, lin.codigo codigo_linha, spo.codigo codigo_spool, iso.peso peso_sei
					, spo.ok_tpspool, spo.ok_matspool, spo.ok_controltub
					, spo.peso
					, spo.visual_solda
					, status_spool_id
					, se.descricao descricao_emissao

					from spools spo
					join linhas lin on lin.id = spo.linha_id
					join isometricos iso on iso.id = lin.isometrico_id
					left join status_emissao se on se.id = iso.status_emissao_id 
					where 1=1
		";
		$sql_peso = "select sum(spo.peso) as peso
					from spools spo
					join linhas lin on lin.id = spo.linha_id
					join isometricos iso on iso.id = lin.isometrico_id
					where 1=1
		";
		$sql_peso_iso = "select sum(iso.peso) as peso
					from isometricos iso 
					where 1=1
		";

		if(Input::get('status_emissao_id', Input::old('status_emissao_id')) != ''){
			$sql .= " and status_emissao_id ".Input::get('status_emissao_id', Input::old('status_emissao_id'));
			$sql_peso .= " and status_emissao_id ".Input::get('status_emissao_id', Input::old('status_emissao_id'));
			$sql_peso_iso .= " and status_emissao_id ".Input::get('status_emissao_id', Input::old('status_emissao_id'));
		}

		$visual_solda = Input::get('visual_solda', Input::old('visual_solda'));
		$where_spools = "";
		if( $visual_solda!= ''){
			if($visual_solda == 'sim'){
				$sql .= " and visual_solda is not null and visual_solda <> '0000-00-00' ";
				$sql_peso .= " and visual_solda is not null and visual_solda <> '0000-00-00' ";
				$where_spools .= " and s.visual_solda is not null and visual_solda <> '0000-00-00'";
			}
			else{
				$sql .= " and (visual_solda is null or visual_solda = '0000-00-00') ";
				$sql_peso .= " and (visual_solda is null or visual_solda = '0000-00-00') ";
				$where_spools .= " and (s.visual_solda is null or visual_solda = '0000-00-00')";
			}
		}

		$unidade_prod_id = Input::get('unidades_prod_id', Input::old('unidade_prod_id'));
		if( $unidade_prod_id!= ''){
			$sql .= " and unidade_prod_id = ".$unidade_prod_id;
			$sql_peso .= " and unidade_prod_id = ".$unidade_prod_id;
			$sql_peso_iso .= " and unidade_prod_id =".$unidade_prod_id;
		}

		$ok_tpspool = Input::get('ok_tpspool', Input::old('ok_tpspool'));
		if( $ok_tpspool!= ''){
			$sql .= " and coalesce(spo.ok_tpspool, 0) = ".$ok_tpspool;
			$sql_peso .= " and coalesce(spo.ok_tpspool, 0) = ".$ok_tpspool;
			$where_spools .= " and coalesce(s.ok_tpspool, 0) = ".$ok_tpspool;
		}

		$ok_matspool = Input::get('ok_matspool', Input::old('ok_matspool'));
		if( $ok_matspool!= ''){
			$sql .= " and coalesce(spo.ok_matspool, 0) = ".$ok_matspool;
			$sql_peso .= " and coalesce(spo.ok_matspool, 0) = ".$ok_matspool;
			$where_spools .= " and coalesce(s.ok_matspool, 0) = ".$ok_matspool;
		}

		$ok_controltub = Input::get('ok_controltub', Input::old('ok_controltub'));
		if( $ok_controltub!= ''){
			$sql .= " and coalesce(spo.ok_controltub, 0) = ".$ok_controltub;
			$sql_peso .= " and coalesce(spo.ok_controltub, 0) = ".$ok_controltub;
			$where_spools .= " and coalesce(s.ok_controltub, 0) = ".$ok_controltub;
		}

		$pendencia = Input::get('pendencia', Input::old('pendencia'));
		if( $pendencia == ''){
			$sql .= " and coalesce(pendencia, '') = '' ";
			$sql_peso .= " and coalesce(pendencia, '') = '' ";
			$where_spools .= " and coalesce(pendencia, '') = '' ";
		}

		$col_q = Input::get('col_q', Input::old('col_q'));
		if( $col_q == ''){
			$sql .= " and col_q ='0' ";
			$sql_peso .= " and col_q ='0' ";
			$where_spools .= " and col_q ='0' ";
		}

		$col_r = Input::get('col_r', Input::old('col_r'));
		if( $col_r == ''){
			$sql .= " and col_r is null ";
			$sql_peso .= " and col_r is null ";
			$where_spools .= " and col_r is null";
		}


		if($where_spools != ""){
			$sql_peso_iso .= " and id in (select isometrico_id from linhas l, spools s where s.linha_id = l.id ".$where_spools.")";
		}

		$sql .= " order by iso.codigo, lin.codigo, spo.codigo";
		$spools = DB::select($sql);

		$peso_total = DB::select($sql_peso);
		$peso_total = $peso_total[0]->peso;
		$peso_total = number_format($peso_total, 2, ",", "." );

		$peso_total_sei = DB::select($sql_peso_iso);
		$peso_total_sei = $peso_total_sei[0]->peso;
		$peso_total_sei = number_format($peso_total_sei, 2, ",", "." );

		$status_emissao = StatusEmissao::select(DB::raw("concat ('igual a',' ',descricao) as descricao, concat('= ',id) as id"))
						->orderBy('id', 'asc')
						->lists('descricao','id')
			;
		$status_emissao_dif = StatusEmissao::select(DB::raw("concat ('diferente de',' ',descricao) as descricao, concat('<> ',id) as id"))
						->orderBy('id', 'asc')
						->lists('descricao','id')
			;
		$status_emissao = array_merge(array('' => '(todos)'), $status_emissao, $status_emissao_dif);
		$unidades_prod = UnidadeProd::select(DB::raw('concat (codigo,"-",descricao) as codigo ,id'))
						->lists('codigo', 'id')
			;
		// $unidades_prod = array_merge(array('' => '(todos)'), $unidades_prod);
		return View::make('spools_verificacao')
						->with('spools', $spools)
						->with('unidades_prod', $unidades_prod)
						->with('status_emissao', $status_emissao)
						->with('peso_total', $peso_total)
						->with('peso_total_sei', $peso_total_sei)
			;
	}
	
	public function postVerificacao()
	{
		return Redirect::to('spool/verificacao')->withInput();
		
	}

	public function getMateriais()
	{
		$status_spool = DB::table('status_spool')
						->orderBy('id', 'asc')
						->lists('descricao','id')
			;
		$unidades_prod = UnidadeProd::select(DB::raw('concat (codigo,"-",descricao) as codigo ,id'))
						->lists('codigo', 'id')
			;
		$spools = array();
		return View::make('spoolsMateriaisQuery')
						->with('unidades_prod', $unidades_prod)
						->with('status_spool', $status_spool)
			;
	}
	
	public function postMateriaisOld()
	{
		Input::flash();
		$spools = Spool::join('linhas', 'linhas.id', '=', 'spools.linha_id');
		$spools->join('isometricos', 'isometricos.id', '=', 'linhas.isometrico_id');
		$spools->join('itens_necessarios', 'itens_necessarios.spool_id', '=', 'spools.id');
//		$spools->with('linha.isometrico');
		$peso_total = Spool::join('linhas', 'linhas.id', '=', 'spools.linha_id');
		$peso_total->join('isometricos', 'isometricos.id', '=', 'linhas.isometrico_id');


//		$sql = "linha_id in (select l.id 
//							   from linhas l, isometricos i 
//							  where l.isometrico_id = i.id 
//							    and i.status_emissao_id = 1 
//								and coalesce(i.pendencia, '') = '')
//				and visual_solda is null ";
//		$spools->whereRaw($sql);
//		$peso_total->whereRaw($sql);
		
		if(Input::get('unidade_prod_id') != ''){
			$sql = "linha_id in (
							select l.id 
							  from linhas l
							     , isometricos i 
						     where l.isometrico_id = i.id 
							   and i.unidade_prod_id = ".Input::get('unidade_prod_id')."
								 )";
			$spools->whereRaw($sql);
			$peso_total->whereRaw($sql);
		}
		
//		if(Input::get('status_spool_id') == '1'){
//			$sql = "(status_spool_id is null or status_spool_id = 1)";
//		}
//		else{
			$sql = "status_spool_id = ".Input::get('status_spool_id');
//		}
		$spools->whereRaw($sql);
		$peso_total->whereRaw($sql);
		
		if(Input::get('so_com_saldo') != ''){
			$sql = "spools.id not in (select distinct s.id
								from spools s
								join itens_necessarios itn on itn.spool_id = s.id
								join itens it on it.id = itn.item_id
								where coalesce(saldo,0) < itn.qtd
								)";
			$spools->whereRaw($sql);
			$peso_total->whereRaw($sql);
		}

		$spools = $spools->get();
		$peso_total = $peso_total->sum('spools.peso');
//		$peso_total = $peso_total / 1000;
		$peso_total = number_format($peso_total, 2, ",", "." );
		$unidades_prod = UnidadeProd::select(DB::raw('concat (codigo,"-",descricao) as codigo ,id'))
						->lists('codigo', 'id')
			;
		$status_spool = DB::table('status_spool')
						->orderBy('ordem', 'asc')
						->lists('descricao','id')
			;
		return View::make('spoolsMateriais')
						->with('spools', $spools)
						->with('unidades_prod', $unidades_prod)
						->with('status_spool', $status_spool)
						->with('peso_total', $peso_total)
			;
	}
	
	public function postMateriais()
	{
		Input::flash();
		$sql_valv = "";
		if(Input::get('valvulas') == ''){
			$sql_valv = " and i.codigo not like 'V%' and i.codigo not like 'HV%'";
			$sql_valv_count = " and itens.codigo not like 'V%' and itens.codigo not like 'HV%' and spo.codigo not like '%-V__'";
		}

		$sql = "select spo.id, iso.codigo codigo_isometrico, lin.codigo codigo_linha, spo.codigo codigo_spool
					, (select count(*) from itens_necessarios, itens where itens.id = itens_necessarios.item_id and spool_id = spo.id ".$sql_valv_count.") qtd_itens
					, spo.peso
					, i.codigo codigo_item
					, i.descricao descricao_item
					, itn.qtd
					, saldo
					, status_spool_id
					, (select sum(qtd) from itens_comprados where item_id = i.id) qtd_comprada
					, (select sum(qtd) from itens_solicitados where item_id = i.id) qtd_solicitada
					from 
						spools spo
					join linhas lin on lin.id = spo.linha_id
					join isometricos iso on iso.id = lin.isometrico_id
					join itens_necessarios itn on itn.spool_id = spo.id
					join itens i on i.id = itn.item_id
					where spo.codigo not like '%-V__'
		".$sql_valv;
		$sql_peso = "select sum(spo.peso) as peso
					from spools spo
					join linhas lin on lin.id = spo.linha_id
					join isometricos iso on iso.id = lin.isometrico_id
					where spo.codigo not like '%-V__'
		";

		$sql .= " and status_spool_id = ".Input::get('status_spool_id');
		$sql_peso .= " and status_spool_id = ".Input::get('status_spool_id');
//		}
		if(Input::get('unidade_prod_id') != ''){
			$sql .= " and iso.unidade_prod_id = ".Input::get('unidade_prod_id');
			$sql_peso .= " and iso.unidade_prod_id = ".Input::get('unidade_prod_id');
		}

		
		if(Input::get('so_com_saldo') != ''){
			$sql .= " and spo.id not in (select distinct s.id
								from spools s
								join itens_necessarios itn on itn.spool_id = s.id
								join itens it on it.id = itn.item_id
								where coalesce(saldo,0) < itn.qtd
								)";
			$sql_peso .= " and spo.id not in (select distinct s.id
								from spools s
								join itens_necessarios itn on itn.spool_id = s.id
								join itens it on it.id = itn.item_id
								where coalesce(saldo,0) < itn.qtd
								)";
		}
		$spools = DB::select($sql);
		$peso_total = DB::select($sql_peso);

//		$spools = $spools->get();
		$peso_total = $peso_total[0]->peso;
//		$peso_total = $peso_total / 1000;
		$peso_total = number_format($peso_total, 2, ",", "." );
		$unidades_prod = UnidadeProd::select(DB::raw('concat (codigo,"-",descricao) as codigo ,id'))
						->lists('codigo', 'id')
			;
		$status_spool = DB::table('status_spool')
						->orderBy('ordem', 'asc')
						->lists('descricao','id')
			;
		return View::make('spoolsMateriaisQuery')
						->with('spools', $spools)
						->with('unidades_prod', $unidades_prod)
						->with('status_spool', $status_spool)
						->with('peso_total', $peso_total)
			;
	}
	
	public function getFabricadossemdesenho()
	{
		$spools = Spool::whereNull('revisao')
						->whereNotNull('visual_solda')
						->where('visual_solda', '<>', '0000-00-00')
						->get();
		return View::make('spoolsFabricadosForaProjeto')->with('spools', $spools);
	}
	
	public function postFabricadossemdesenho()
	{
		$spools = Spool::whereNull('revisao')
						->whereNotNull('visual_solda')
						->where('visual_solda', '<>', '0000-00-00')
						->get();
		return View::make('spoolsFabricadosForaProjeto')->with('spools', $spools);
	}
	
	public function getDesenhadosfabricados()
	{
		$spools = Spool::whereNotNull('revisao')
						->whereNotNull('revisao_sei')
						->whereNotNull('visual_solda')
						->where('visual_solda', '<>', '0000-00-00')
						->get();
		return View::make('spoolsDetalhadosFabricados')->with('spools', $spools);
	}
	
	public function postDesenhadosfabricados()
	{
		$spools = Spool::whereNull('revisao')
						->whereNotNull('visual_solda')
						->where('visual_solda', '<>', '0000-00-00')
						->get();
		return View::make('spoolsDetalhadosFabricados')->with('spools', $spools);
	}
	
	public function postAlterestatus($pid, $pstatus_spool_id)
	{
		$spool = Spool::find($pid);
		$data = $spool->ColoqueNoStatusSpool($pstatus_spool_id);
		return json_encode($data);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		//
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}
	
	public function getImporteRelacaoSpools($id){
		$caminho = "../public/uploads/spo/".$id;
		$plan= new Planilha();
		$plan->abrir($caminho, 1, 22);
		while (!$plan->eof()) {
			$isom = Isometrico::Valido($plan->col(21));
			$lin = Linha::Valido($plan->col(5), $isom->id);
		}
	}
	
	public function getImporte($id)
	{
		$linhaInicial = 6;
		$caminho = "../public/uploads/spo/".$id;
		if (file_exists($caminho)) {
			$handle = fopen($caminho, "r");
			$ind = 1;
			$buffer = fgets($handle);
			$buffer = utf8_encode(fgets($handle));
			while (!feof($handle) || strlen($buffer) >= 10) {
				if($ind >= $linhaInicial && trim(str_replace("\n", "", str_replace("\t", "", $buffer))) != ''){
					if(substr_count($buffer,"\t") < 5){
						while(substr_count($buffer,"\t") < 5){
							$buffer .= utf8_encode(fgets($handle));
							$ind++;
						}
					}
					$colunas = explode("\t", $buffer);

					if(str_contains($colunas[0], 'Total')){
						//skip
					}
					else{
						if($colunas[0] != ''){
							$isometrico = Isometrico::Valido($colunas[0]);							
						}

						if($colunas[1] != ''){
							$linha = Linha::Valido($colunas[1], $isometrico->id);							
						}
						if($colunas[2] != ''){
							$codigo_linha = $colunas[2];
							if(substr($codigo_linha,0, 1) == '"'){
								$codigo_linha = substr($codigo_linha, 1, strlen($codigo_linha)-2);
							}
							$codigo_linha = str_replace('""', '"', $codigo_linha);
							$spool = Spool::Valido($codigo_linha, $linha->id);							
						}
						unset($item);
						$item = Item::where('codigo', '=', $colunas[4])->first();
						if (isset($item)){
							$spool->AdicionarItemNecessario($item->id, $colunas[5] , $colunas[6]);								
						}

					}
				}
				$buffer = utf8_encode(fgets($handle));
				$ind++;
			}
			fclose($handle);
		}
		else{
			print 'Arquivo '.$caminho.' não encontrado';
		}
	}

	public function getRma($pid)
	{
		$spool = Spool::find($pid);
		$response = Response::make(View::make('reqMaterialAlmoxarifado')
			->with('spool', $spool))
		;
		$response->headers->set('Pragma', 'public');
		$response->headers->set('Expires', '0');
		$response->headers->set('Cache-Control', 'must-revalidate, post-check=0, pre-check=0');
		$response->headers->set('Content-Description', 'File Transfer');
		$response->headers->set('Content-Type', 'application/force-download');
		$response->headers->set('Content-Disposition', 'attachment;filename=rma.xls');
		;
		return $response;
	}

}