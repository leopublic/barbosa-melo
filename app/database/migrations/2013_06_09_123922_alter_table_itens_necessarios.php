<?php

use Illuminate\Database\Migrations\Migration;

class AlterTableItensNecessarios extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('itens_necessarios', function($table)
		{
			$table->integer('item_id')->unsigned()->nullable();
			$table->foreign('item_id')->references('id')->on('itens');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		
	}

}