<?php
use Illuminate\Database\Migrations\Migration;
class CreateTableSubunidade extends Migration {
	public function up()
	{
		Schema::create('sub_unidades', function($table) {
			$table->increments('id');
			$table->string('descricao');
			$table->timestamps();
		});
	}

	public function down()
	{
		Schema::drop('sub_unidades');
	}
}