<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddControleImportacoesToSpoolsTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('spools', function(Blueprint $table) {
            $table->tinyInteger('ok_matspool')->unsigned()->null();
            $table->tinyInteger('ok_tpspool')->unsigned()->null();
            $table->tinyInteger('ok_controltub')->unsigned()->null();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('spools', function(Blueprint $table) {
            $table->dropColumn('ok_matspool');
            $table->dropColumn('ok_tpspool');
            $table->dropColumn('ok_controltub');            
        });
    }

}
