<?php
class ImportacaoRmController extends BaseController {
	/**
	 * 
	 */
	
	public function getIndex()
	{
		$importacoes = ImportacaoRm::orderBy('created_at', 'desc')->get();
		return View::make('importacaospool')
				->with('importacoes', $importacoes);
		
	}
	
	public function postIndex()
	{
		$imp = new ImportacaoRm();
		$imp->nome_arquivo = Input::file('arquivo')->getClientOriginalName();
		$imp->save();
		
		Input::file('arquivo')->move($imp->get_pasta(), $imp->id);
		Session::flash('msg', 'Planilha importada com sucesso.');
		return Redirect::to('importacaospool');
	}
	
	public function getImporte($id)
	{
		$imp = ImportacaoSpool::findOrFail($id);
		if($imp->tipo == 1){
			$imp->ImportarTipo1();
		}
		else{
			$imp->ImportarTipo2();
		}
//		$plan = Planilha_rm::ImportarCsv('../public/uploads/'.$id);
//		Session::flash('msg', 'Planilha importada com sucesso.');
//		return Redirect::to('ImportacaoRm');
		Session::flash('msg', 'Planilha carregada com sucesso.');
		return Redirect::to('importacaorm');		
	}

	public function carregueNovo()
	{
		$importacoes = ImportacaoRm::all();
		return View::make('importacao_rm')
				->with('importacoes', $importacoes);
	}
	
	public function postNovo()
	{
		$imp = new ImportacaoRm();
		$imp->nome_arquivo = Input::file('arquivo')->getClientOriginalName();
		$imp->save();
		
		Input::file('arquivo')->move('../public/uploads/', $imp->id);
		Session::flash('msg', 'Importação carregada com sucesso.');
		return Redirect::to('ImportacaoRm');
	}
	
	public function importeCsv($id)
	{
	}
}