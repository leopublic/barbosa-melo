@extends('layout')

@section('conteudo')
<table>
	<colgroup>
		<col width="120px">
		<col width="120px">
		<col width="120px">
		<col width="120px">
		<col width="auto">
		<col width="80px">
		<col width="80px">
		<col width="80px">
		<col width="80px">
		<col width="80px">
		<col width="80px">
		<col width="80px">
	</colgroup>
	<tbody>
		<tr>
			<th colspan="3">&nbsp;</th>
			<th colspan="6">REQUISIÇÃO DE MATERIAL AO ALMOXARIFADO</th>
			<th colspan="4">RMA N&ordm;</th>
		</tr>
		<tr>
			<th>ITEM</th>
			<th colspan="2">TAG/LM/FD/AF/DESENHO</th>
			<th colspan="4">DESCRIÇÃO DO MATERIAL</th>
			<th colspan="4" rowspan="2">QUANTIDADES</th>
			<th colspan="2" rowspan="2">REFERÊNCIAS</th>
		</tr>
		<tr>
			<th>UNID,</th>
			<th>REQUISITADO</th>
			<th>ENTREGUE</th>
			<th>DEVOLVIDO</th>
			<th>NF</th>
			<th>RIR</th>
		</tr>
		<th>
		@foreach ($spool->item_necessario as $item_necessario)
		<tr>
			<td >{{ $item_necessario->item->codigo }}</td>
			<td colspan="2">{{ $item_necessario->item->descricao }}</td>
			<td colspan="4">{{ $item_necessario->item->unidade->descricao }}</td>
			<td >{{ $item_necessario->qtd }}</td>
			<td >&nbsp;</td>
			<td >&nbsp;</td>
			<td >&nbsp;</td>
			<td >&nbsp;</td>
			<td >&nbsp;</td>
		</tr>
		@endforeach
		<tr>
			<td colspan="5">REQUISITANTE</td>
			<td colspan="2">RETIRADA DO ALMOXARIFADO</td>
			<td colspan="6">APLICAÇÃO</td>
		</tr>
		<tr>
			<td colspan="3">EMPRESA:</td>
			<td colspan="2">SETOR:</td>
			<td colspan="2">NOME: </td>
			<td colspan="6">{{ $spool->linha->isometrico->codigo }}</td>
		</tr>
		<tr>
			<td colspan="3">NOME:</td>
			<td colspan="2">FUNÇÃO:</td>
			<td colspan="2">FUNÇÃO: </td>
			<td colspan="6">&nbsp;</td>
		</tr>
		<tr>
			<td colspan="5">DATA:</td>
			<td colspan="2">DATA:</td>
			<td colspan="6">&nbsp;</td>
		</tr>
		<tr>
			<td colspan="5">APROVADO:</td>
			<td colspan="2">ASSINATURA:</td>
			<td colspan="6">&nbsp;</td>
		</tr>
		<tr>
			<td colspan="5">ENTREGA DOS MATERIAIS ALMOXARIFADO CBMSA</td>
			<td colspan="8">DEVOLUÇÃO DOS MATERIAIS</td>
		</tr>
		<tr>
			<td colspan="2">NOME</td>
			<td colspan="2">ASSINATURA</td>
			<td colspan="1">DATA</td>
			<td colspan="1">REQUISITANTE / NOME / DATA</td>
			<td colspan="4">ALMOXARIFADO / NOME / DATA</td>
			<td colspan="3">ASSINATURA</td>
		</tr>
		<tr>
			<td colspan="2">&nbsp;</td>
			<td colspan="2">&nbsp;</td>
			<td colspan="1">&nbsp;</td>
			<td colspan="1">&nbsp;</td>
			<td colspan="4">&nbsp;</td>
			<td colspan="3">&nbsp;</td>
		</tr>
	</tbody>
</table>
@stop