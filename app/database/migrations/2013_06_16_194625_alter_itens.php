<?php

use Illuminate\Database\Migrations\Migration;

class AlterItens extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('itens', function($table)
		{
			$table->integer('familia_material_id')->unsigned()->nullable();
		});
		
		Schema::table('itens', function($table)
		{
			$table->foreign('familia_material_id')->references('id')->on('familias_material');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
	}
}