<?php

use Illuminate\Database\Migrations\Migration;

class CreateTableUsuarios extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('usuarios', function($table)
		{
			$table->engine = 'InnoDB';
			$table->increments('id');
			$table->string('nome', 200)->unique();
			$table->string('username', 16);
			$table->string('password', 64);
			$table->string('email', 200);
			$table->timestamps();
		});	
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('usuarios');
	}

}