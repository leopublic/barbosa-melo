<?php
class ImportacaoIsometricoController extends \BaseController {
	
	public function getIndex()
	{
		$importacoes = ImportacaoIsometrico::orderBy('created_at', 'desc')->get();;
		return View::make('importacaoisometrico')
				->with('importacoes', $importacoes);
		
	}
	
	public function postIndex()
	{
		$imp = new ImportacaoIsometrico();
		$imp->nome_arquivo = Input::file('arquivo')->getClientOriginalName();
		$imp->usuario_id = Auth::user()->id;
		$imp->save();
		
		Input::file('arquivo')->move($imp->get_pasta(), $imp->id);
		Session::flash('msg', 'Importação carregada com sucesso.');
		return Redirect::to('importacaoiso');
	}
	
	public function getImporte($id)
	{
		$imp = ImportacaoIsometrico::findOrFail($id);
		$imp->Importe();
		Session::flash('msg', 'Planilha carregada com sucesso.');
		return Redirect::to('importacaoiso');
	}
	
	public function getArquivo($pid)
	{
		$imp = ImportacaoIsometrico::find($pid);
		return Response::download($imp->get_caminhoArquivo(), $imp->nome_arquivo);
	}
}
