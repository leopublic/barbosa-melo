<?php
class ImportacaoSpoolController extends \BaseController {

	public function getIndex()
	{
		$importacoes = ImportacaoSpool::orderBy('created_at', 'desc')->get();
		return View::make('importacaospool')
				->with('importacoes', $importacoes);
		
	}
	
	public function postIndex()
	{
		$imp = new ImportacaoSpool();
		$imp->nome_arquivo = Input::file('arquivo')->getClientOriginalName();
		$imp->usuario_id = Auth::user()->id;
		$imp->tipo = Input::get('tipo');
		$imp->save();
		
		Input::file('arquivo')->move($imp->get_pasta(), $imp->id);
		Session::flash('msg', 'Planilha importada com sucesso.');

		$importacoes = ImportacaoSpool::orderBy('created_at', 'desc')->get();
		return View::make('importacaospool')
				->with('importacoes', $importacoes);
	}
	
	public function getImporte($id)
	{
		$imp = ImportacaoSpool::findOrFail($id);
		if($imp->tipo == 1){
			$imp->ImportarTipo1();
		}
		elseif($imp->tipo == 2){
			$imp->ImportarTipo2();
		}
		elseif($imp->tipo == 4){
			$imp->ImportarTPSPOOLExcel();
		}
		elseif($imp->tipo == 5){
			$imp->ImportarMATSPOOLExcel();
		}
		else{
			$imp->ImportarTipo3();
		}
		Session::flash('msg', 'Planilha carregada com sucesso.');
		return Redirect::to('importacaospool');		
	}
	
	public function getImportematspoolemlote()
	{
		set_time_limit(0);
		$caminho = "../public/downloads/ftp.m2software.com.br/MATSPOOL";
		$objects = new RecursiveIteratorIterator(new RecursiveDirectoryIterator($caminho));
		foreach($objects as $fileInfo){
			if(!$fileInfo->isDir() && ($fileInfo->getExtension() == 'xls' || $fileInfo->getExtension() == 'xlsx') ){
				print '<br/>'.$fileInfo->getFilename();
				$imp = new ImportacaoSpool();
				$imp->nome_arquivo = $fileInfo->getFilename();
				$imp->usuario_id = Auth::user()->id;
				$imp->tipo = 5;
				$imp->save();
				copy($fileInfo->getPathname(), $imp->get_pasta() . '/' . $imp->id);
//				$imp->ImportarMATSPOOLExcel();
			}
		}
	}
	
	public function getImportetpspoolemlote()
	{
		set_time_limit(0);
		$caminho = "../public/downloads/ftp.m2software.com.br/TPSPOOL";
		$objects = new RecursiveIteratorIterator(new RecursiveDirectoryIterator($caminho));
		foreach($objects as $fileInfo){
			if(!$fileInfo->isDir() && ($fileInfo->getExtension() == 'xls' || $fileInfo->getExtension() == 'xlsx') ){
				print '<br/>'.$fileInfo->getFilename();
				$imp = new ImportacaoSpool();
				$imp->nome_arquivo = $fileInfo->getFilename();
				$imp->usuario_id = Auth::user()->id;
				$imp->tipo = 4;
				$imp->save();
				copy($fileInfo->getPathname(), $imp->get_pasta() . '/' . $imp->id);
//				$imp->ImportarTPSPOOLExcel();
			}
		}
	}
	
	public function getProcessependentes($tipo)
	{
		set_time_limit(0);		
		$importacoes = ImportacaoSpool::where('tipo', '=', $tipo)
//						->where('data_importacao', 'is', 'null')
						->orderBy('id', 'asc')
						->get()
				;
		foreach($importacoes as $imp){
			if($imp->tipo == 4){
				$imp->ImportarTPSPOOLExcel();
			}elseif($imp->tipo == 5){
				$imp->ImportarMATSPOOLExcel();
			}
		}
		Session::flash('msg', 'Importações pendentes processadas com sucesso!');
		return Redirect::to('importacaospool');		
	}
	
	public function getArquivo($pid)
	{
		$imp = ImportacaoSpool::find($pid);
		return Response::download($imp->get_caminhoArquivo(), $imp->nome_arquivo);
	}
}