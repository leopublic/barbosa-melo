<?php
/**
 * @property int $id Chave
 * @property string $descricao Descricao
 */
class Material extends Eloquent{
	protected $table = 'especs';
	/**
	 * Cadastra um novo ou recupera se houver. 
	 * Utilizado nas importacoes
	 * @param string $pdescricao Descricao da area que se deseja cadastrar.
	 * @return Material Retorna uma instancia de Material
	 */
	public static function Valido($pdescricao)
	{
		if($pdescricao == ''){
			$obj = new Material();
		}
		else{
			$obj = Material::where('descricao', '=', $pdescricao)->first();
			if(!isset($obj)){
				$obj = new Material();
				$obj->descricao = $pdescricao;
				$obj->save();
			}
		}
		return $obj;
	}	
}