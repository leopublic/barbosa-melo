<?php
class ImportacaoEstoqueController extends \BaseController {

	public function getIndex()
	{
		$importacoes = ImportacaoEstoque::orderBy('created_at', 'desc')->get();
		return View::make('importacaoestoque')
				->with('importacoes', $importacoes);
		
	}
	
	public function postIndex()
	{
		$imp = new ImportacaoEstoque();
		$imp->nome_arquivo = Input::file('arquivo')->getClientOriginalName();
//		$imp->usuario_id = Auth::
		$imp->tipo = Input::get('tipo');
		$imp->save();
		
		Input::file('arquivo')->move($imp->get_pasta(), $imp->id);
		Session::flash('msg', 'Planilha importada com sucesso.');
		return Redirect::to('importacaoestoque');
	}
	
	public function getImporte($id)
	{
		$imp = ImportacaoEstoque::findOrFail($id);
		$imp->Importar();
		Session::flash('msg', 'Planilha carregada com sucesso.');
		return Redirect::to('importacaoestoque');		
	}
}