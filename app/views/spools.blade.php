@extends('layout')

@section('conteudo')
<div class="interno">
	<h1>Spools</h1>
	{{ Form::open() }}
	<table class="listagem">
		<colgroup>
			<col width="120px">
			<col width="120px">
			<col width="120px">
			<col width="120px">
			<col width="auto">
			<col width="80px">
			<col width="80px">
			<col width="80px">
			<col width="80px">
		</colgroup>
		<thead>
			<tr>
				<th>Isométrico</th>
				<th>Linha</th>
				<th>Spool</th>
				<th>Item</th>
				<th>Descrição</th>
				<th class="r">Qtd projeto</th>
				<th class="r">Qtd RM</th>
				<th class="r">Qtd comprada</th>
				<th class="r">Saldo em estoque</th>
			</tr>			
		</thead>
		<tbody>
			@foreach ($spools as $spool)
			<tr style="border-top: solid 2px #777;">
				<td class="iso" rowspan="{{ count($spool->ItensDoSpool()) }}">{{ $spool->linha->isometrico->codigo }}</td>
				<td class="iso" rowspan="{{ count($spool->ItensDoSpool()) }}">{{ $spool->linha->codigo }}</td>
				<td class="iso" rowspan="{{ count($spool->ItensDoSpool()) }}">{{ $spool->codigo }}</td>
				@foreach ($spool->ItensDoSpool() as $item)
					<td>{{ $item->codigo }}</td>
					<td>{{ $item->descricao }}</td>
					<td class="r">{{ $item->qtd }}</td>
					<td class="r">{{ $item->qtd_solicitada }}</td>
					<td class="r">{{ $item->qtd_comprada }}</td>
					<td class="r">{{ $item->saldo }}</td>
				</tr>
				@endforeach			
			@endforeach			
		</tbody>
	</table>
	{{ Form::close() }}	
</div>
@stop