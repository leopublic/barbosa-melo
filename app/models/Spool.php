<?php
/**
 * @property int $id Chave
 * @property string $descricao Descricao
 * @property decimal $area
 * @property decimal $comprimento
 * @property integer $total_junta
 * @property integer $total_junta_pipe
 * @property integer $total_junta_pipe_ajustada
 * @property integer $total_junta_pipe_soldada
 * @property integer $total_junta_pipe_aprovada
 * @property integer $total_junta_pipe_
 * @property string $revisao_sei
 * @property date $liberacao_fab
 * @property date $corte
 * @property date $ajuste
 * @property date $solda
 * @property date $visual_solda
 * @property date $liberacao
 * @property date $inspetor_dim
 * @property date $dimensional
 * @property string $relatorio_dim
 * @property date $pintura_fundo
 * @property date $pintura_acabamento
 * @property date $status_spool_id		Status de produção do spool
 */
class Spool extends Eloquent{
	protected $table = 'spools';


	public function linha()
	{
		return $this->belongsTo('Linha');
	}

	public function item()
	{
		return $this->belongsToMany('Item', 'itens_necessarios', 'item_id', 'spool_id');	
	}
	
	public function itemnecessario()
	{
		return $this->hasMany('ItemNecessario');
	}
	/**
	 * Cadastra uma nova ou recupera se houver. 
	 * Utilizado nas importacoes
	 * @param string $pdescricao Descricao da area que se deseja cadastrar.
	 * @return Area Retorna uma instancia de Area
	 */
	public static function Valido($pcodigo, $plinha_id = 'null' )
	{
		$obj = Spool::where('codigo', '=', $pcodigo)->first();
		if(!isset($obj)){
			$obj = new Spool();
			$obj->codigo = $pcodigo;
			$obj->linha_id = $plinha_id;
			$obj->save();
		}
		return $obj;
	}
	
	public function AdicionarItemNecessario($pitem_id, $pqtd, $ppeso, $punidade_id)
	{
		$obj = ItemNecessario::where('item_id', '=', $pitem_id)
					->where('spool_id', '=', $this->id)
					->first();
		if(!isset($obj)){
			$obj = new ItemNecessario();
			$obj->item_id = $pitem_id;
			$obj->spool_id = $this->id;
		}
		$obj->unidade_id = $punidade_id;
		$obj->qtd = $pqtd;
		$obj->peso = $ppeso;
		$obj->save();
		return $obj;
	}

	public function QtdItensDoSpool(){
		$itens = DB::table('itens_necessarios')
						->where('spool_id', '=', $this->id)
						->count()
				;
		return $itens;
	}

	public function ItensDoSpool(){
		$itens = DB::select("select 
				  i.id, i.codigo, i.saldo
				, i.descricao, sum(itn.qtd) qtd, sum(itn.peso) peso
				, (select sum(qtd) from itens_solicitados where item_id = i.id) qtd_solicitada
				, (select sum(qtd) from itens_comprados where item_id = i.id) qtd_comprada
				, (select sum(qtd) from itens_recebidos where item_id = i.id) qtd_recebida
			from itens_necessarios itn
			join itens i on i.id = itn.item_id 
			where itn.spool_id = ".$this->id."
			group by i.id, i.codigo, i.saldo, i.descricao
			");
		
		return $itens;
	}
	
	public function StatusLiberadoParaFabricacao()
	{
		$this->ColoqueNoStatusSpool(1);
	}
	
	public function StatusReservado()
	{
		$this->ColoqueNoStatusSpool(7);
	}
	
	public function StatusEmFabricacao()
	{
		$this->ColoqueNoStatusSpool(2);
	}
		
	public function StatusFabricado()
	{
		$this->ColoqueNoStatusSpool(3);
	}
	
	public function StatusEmPintura()
	{
		$this->ColoqueNoStatusSpool(4);
	}
	
	public function StatusPintado()
	{
		$this->ColoqueNoStatusSpool(5);
	}

	public function ColoqueNoStatusSpool($pstatus_spool_id)
	{
		$data = array();
		if($pstatus_spool_id == 7){
			$this->dt_programacao = date('Y-m-i H:i:s');
			$this->user_id = Auth::user()->id;
		}
		$this->status_spool_id = $pstatus_spool_id;
		
		$this->save();
		
		$data['msg']['tipoMsg'] = 'success';
		if($pstatus_spool_id == 7){
			$data['novoConteudo'] = 'Reservado';			
		}
		elseif($pstatus_spool_id == 1){
			$data['novoConteudo'] = 'Cancelado';			
		}
		elseif($pstatus_spool_id == 4){
			$data['novoConteudo'] = 'Em pintura';			
		}
		
		return $data;
	}

	public function acao()
	{
		$novo_status = '';
		if($this->status_spool_id == '' || $this->status_spool_id == 1){
			$novo_status = 7;
			$titulo = 'Reservar';
		}
		elseif($this->status_spool_id == '7'){
			$novo_status = 1;
			$titulo = 'Cancelar';
		}

		$ret = array();
		if($novo_status != ''){
			$ret[] = array(
				"data" => array(
							"url"		=> "/spool/alterestatus/".$this->id."/".$novo_status
							)
				,"titulo"	=> $titulo
			);
		}
		return $ret;
	}
	
}