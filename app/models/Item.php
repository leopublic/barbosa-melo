<?php
/**
 * @property int $id Chave
 * @property string $descricao Descricao
 * @property string $codigo Codigo do material
 * @property integer $unidade_id Chave da unidade (não obrigatorio)
 * @property integer $familia_material_id Chave da familia do material (não obrigatorio)
 * @property string $descricao_resumida Descricao resumida obtida do controle de itens do spool
 * @property decimal $saldo Quantidade disponivel em estoque
 */
class Item extends Eloquent{
	protected $table = 'itens';
	
	public function unidade()
	{
		return $this->belongsTo('Unidade');
	}
	
	public function itemcomprado()
	{
		$this->hasMany('ItemComprado');
	}
	
	public function itemsolicitado()
	{
		$this->hasMany('ItemSolicitado');
	}
	
	public function itemrecebido()
	{
		$this->hasMany('ItemRecebido');
	}
	
	public function qtdComprada()
	{
		return ItemComprado::where('item_id','=', $this->id)->sum('qtd');
	}
		
	public function qtdRecebida()
	{
		return ItemRecebido::where('item_id','=', $this->id)->sum('qtd');
	}
	/**
	 * Cadastra uma nova ou recupera se houver. 
	 * Utilizado nas importacoes
	 * @param string $pdescricao Descricao da classe que se deseja cadastrar.
	 * @return Area Retorna uma instancia da classe
	 */
	public static function Valido($pdescricao, $pcodigo, $punidade_id, $pfamilia_id)
	{
		$pdescricao = trim($pdescricao);
		if(substr($pdescricao,0,1) == '"'){
			$pdescricao = substr($pdescricao,1,  strlen($pdescricao) - 2);
		}
		$pdescricao = str_replace('""', '"', $pdescricao);			
		$pdescricao = str_replace("\n", '<br/>', $pdescricao) ;
		$obj = Item::where('codigo', '=', $pcodigo)->first();
		if(!isset($obj)){
			$obj = new Item();
			$obj->descricao = $pdescricao;
			$obj->codigo = $pcodigo;
			$obj->unidade_id = $punidade_id;
			$obj->familia_material_id = $pfamilia_id;
			try{
				$obj->save();				
			}
			catch(exception $e){
				throw $e;
			}
		}
		return $obj;
	}
	/**
	 * Cadastra uma nova ou recupera se houver. 
	 * Utilizado nas importacoes
	 * @param string $pdescricao Descricao da classe que se deseja cadastrar.
	 * @return Area Retorna uma instancia da classe
	 */
	public static function ValidoResumido($pdescricao_resumida, $pcodigo)
	{
		$obj = Item::where('codigo', '=', $pcodigo)->first();
		if(!isset($obj)){
			$obj = new Item();
			$obj->codigo = $pcodigo;
			$obj->descricao_resumida = $pdescricao_resumida;
			$obj->save();
		}
		return $obj;
	}
}