<?php

class StatusEmissao extends Eloquent {
	protected $table = 'status_emissao';
    protected $guarded = array();

    public static $rules = array();
	/**
	 * Cadastra um novo ou recupera se houver. 
	 * Utilizado nas importacoes
	 * @param string $pdescricao Descricao da area que se deseja cadastrar.
	 * @return UnidadeProd Retorna uma instancia de UnidadeProd
	 */
	public static function Valido($pdescricao)
	{
		if(trim($pdescricao) == '' || trim($pdescricao) == '-'){
			$obj = new StatusEmissao();
		}
		else{
			$obj = StatusEmissao::where('descricao', '=', $pdescricao)->first();
			if(!isset($obj)){
				$obj = new StatusEmissao();
				$obj->descricao = $pdescricao;
				$obj->save();
			}
		}
		return $obj;
	}	
	
}