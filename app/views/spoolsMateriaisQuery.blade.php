@extends('layout')

@section('conteudo')
<div class="interno">
	{{ Form::open(array("name" => "form", "id" => "form")) }}
	<div class="grid">
		<div class="row">
			<h1 class="slot-0-1-2-3">Programação de Fabricação de Spools</h1>
			<p class="slot-4-5 botaoTopo" style="margin: 0; text-align:right;">
				<i class="icon-filter" onclick="$('#form').submit();" title="Clique para aplicar os filtros"></i>
			</p>
		</div>
	</div>	
	<div class="grid filtro">
		<div class="row">
			<p class="slot-0-1" style="margin-top:20px;">Status:<br/>{{ Form::select('status_spool_id', $status_spool, Input::old('status_spool_id')) }}</p>
			<p class="slot-2-3" style="margin-top:20px;">Unidade:<br/>{{ Form::select('unidade_prod_id', array('' => '(todas)') + $unidades_prod, Input::old('unidade_prod_id')) }}</p>
			<p class="slot-4-5" style="margin-top:20px;">Somente com saldo:<br/>{{ Form::checkbox('so_com_saldo', '1', Input::old('so_com_saldo')) }}</p>
		</div>
		<div class="row">
			<p class="slot-0-1" style="margin-top:20px;">Exibir válvulas:<br/>{{ Form::checkbox('valvulas', '1', Input::old('valvulas')) }}</p>
			<p class="slot-2-3" style="margin-top:20px;">&nbsp;</p>
			<p class="slot-4-5" style="margin-top:20px;">&nbsp;</p>
		</div>
	</div>		
	@if (isset($spools)) 
	<table class="listagem">
		<colgroup>
			<col width="100px">
			<col width="250px">
			<col width="100px">
			<col width="130px">
			<col width="auto">
			<col width="80px">
			<col width="80px">
			<col width="80px">
			<col width="80px">
		</colgroup>
		<thead>
		<tr>
			<th class="c">Ações</th>
			<th style="text-align:left;">
					Spool
					<span style="color: #666;">
					<br/><i class="icon-angle-right" style="margin-left:7px;margin-right:2px;"></i>Linha
					<br/><i class="icon-angle-right" style="margin-left:14px;margin-right:2px;"></i>Isométrico
					</span>
			</th>
			<th class="r">
				Peso(kg)
				@if (isset($peso_total)) 
					<br/>{{ $peso_total }}
				@endif 
			</th>
			<th>Item</th>
			<th>Descrição</th>
			<th class="r">Qtd projeto</th>
			<th class="r">Qtd RM</th>
			<th class="r">Qtd comprada</th>
			<th class="r">Saldo em estoque</th>
		</tr>			
		</thead>	
		<tbody>
		<?php $spool_ant = ''; ?>
		@foreach ($spools as $spool)
			@if ($spool->id != $spool_ant)
			<tr style="border-top: solid 2px #777;">
				<td class="iso c" rowspan="{{ $spool->qtd_itens }}" >
					<?php $spo = new Spool();
						  $spo->id = $spool->id;
						  $spo->status_spool_id = $spool->status_spool_id;
						  $acoes = $spo->acao();
					; ?>
				@foreach ($acoes as $acao)				
					<a class="botao" href="#" data-parameters="{{{ json_encode($acao['data']) }}}" onclick="javascript:AcionaProcesso(this);return false;">{{ $acao['titulo'] }}</a>
				@endforeach								
				</td>
				<td  rowspan="{{ $spool->qtd_itens }}" style="text-align:left;">
						{{ $spool->codigo_spool }}
						<span style="color: #666;">
							<br/><i class="icon-angle-right" style="margin-left:7px;margin-right:2px;"></i>{{ $spool->codigo_linha }}
							<br/><i class="icon-angle-right" style="margin-left:14px;margin-right:2px;"></i>{{ $spool->codigo_isometrico }}
						</span>
					<?php $spool_ant = $spool->id; ?>
				</td>
				<td class="iso r" rowspan="{{ $spool->qtd_itens }}">{{ number_format($spool->peso, 2, ",", ".") }}</td>			
			@else
			<tr style="">
			@endif
				<td>{{ $spool->codigo_item }}</td>
				<td>{{ $spool->descricao_item }}</td>
				<td class="r">{{ number_format($spool->qtd, 2, ",", ".") }}</td>
				<td class="r">{{ number_format($spool->qtd_solicitada, 2, ",", ".") }}</td>
				<td class="r">{{ number_format($spool->qtd_comprada, 2, ",", ".") }}</td>
				<td class="r">{{ number_format($spool->saldo, 2, ",", ".") }}</td>
			</tr>
		@endforeach			
			
		</tbody>
	</table>
	@else
	Selecione uma unidade
	@endif
	{{ Form::close() }}	
</div>
@stop