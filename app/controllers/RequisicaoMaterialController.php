<?php
class RequisicaoMaterialController extends \BaseController {
	public function getIndex()
	{
		$rms = DB::table('requisicoes_material')
					->where('codigo', 'like', 'RM-5400.00-%-2%-%')
					->orderBy('codigo', 'asc')
					->lists('codigo','id')
			;
		return View::make('requisicao_material')
					->with('rms', $rms)
			;
	}
	
	public function postIndex()
	{
		Input::flash();
		$rms = DB::table('requisicoes_material')
					->where('codigo', 'like', 'RM-5400.00-%-2%-%')
					->orderBy('codigo', 'asc')
					->lists('codigo','id')
			;
		
		$itens_solicitados = ItemSolicitado::where('requisicao_material_id', '=', Input::get('rm_id'))
					->with('Item')
					->get()
			;
		return View::make('requisicao_material')
					->with('rms', $rms)
					->with('itens_solicitados', $itens_solicitados)
			;
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}
}