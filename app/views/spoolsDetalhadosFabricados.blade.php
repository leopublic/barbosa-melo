@extends('layout')

@section('conteudo')
<div class="interno">
	<h1>Spools liberados pela SEI que foram fabricados</h1>
	{{ Form::open() }}
	<table class="listagem">
		<thead>
			<tr>
				<th>Isométrico</th>
				<th>Linha</th>
				<th>Spool</th>
				<th class="r">Peso</th>
				<th class="c" title="Revisão do spool na SEI">Revisão (SEI)</th>
				<th class="c" title="Revisão do spool no ControlTub">Revisão (CT)</th>
				<th class="c">Liberação fab.</th>
				<th class="c">Solda</th>
				<th class="c">Visual solda</th>
			</tr>			
		</thead>
		<tbody>
			@foreach ($spools as $spool)
			<tr>
				<td>{{ $spool->linha->isometrico->codigo }}</td>
				<td>{{ $spool->linha->codigo }}</td>
				<td>{{ $spool->codigo }}</td>
				<td class="r">{{ $spool->peso }}</td>
				<td class="c">{{ $spool->revisao }}</td>
				<td class="c">{{ $spool->revisao_sei }}</td>
				<td class="c">{{ $spool->liberacao_fab }}</td>
				<td class="c">{{ $spool->solda }}</td>
				<td class="c">{{ $spool->visual_solda }}</td>
			</tr>
			@endforeach			
		</tbody>
	</table>
	{{ Form::close() }}	
</div>
@stop