<?php

use Illuminate\Database\Migrations\Migration;

class CreateTableRequisicoesMaterial extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('requisicoes_material', function($table)
		{
			$table->engine = 'InnoDB';
			$table->increments('id');
			$table->integer('tipo_requisicao_id')->unsigned();
			$table->integer('area_id')->unsigned();
			$table->string('codigo', 30)->unique();
			$table->date('data_cadastro');
			$table->integer('revisao');
			$table->date('data_revisao');
			$table->timestamps();
		});	
		
		Schema::table('requisicoes_material', function($table)
		{
			$table->foreign('tipo_requisicao_id')->references('id')->on('tipos_requisicao');
			$table->foreign('area_id')->references('id')->on('areas');
		});	
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('requisicoes_material');
	}

}