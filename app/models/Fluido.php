<?php
/**
 * @property int $id Chave
 * @property string $descricao Descricao
 */
class Fluido extends Eloquent{
	protected $table = 'fluidos';
	/**
	 * Cadastra um novo ou recupera se houver. 
	 * Utilizado nas importacoes
	 * @param string $pdescricao Descricao da area que se deseja cadastrar.
	 * @return Fluido Retorna uma instancia de Fluido
	 */
	public static function Valido($pdescricao)
	{
		if($pdescricao == ''){
			$obj = new Fluido();
		}
		else{
			$obj = Fluido::where('descricao', '=', $pdescricao)->first();
			if(!isset($obj)){
				$obj = new Fluido();
				$obj->descricao = $pdescricao;
				$obj->save();
			}
		}
		return $obj;
	}	
}