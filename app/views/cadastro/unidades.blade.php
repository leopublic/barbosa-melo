@extends('layout')

@section('conteudo')
<div class="interno">
	<h1>Unidades</h1>
	{{ Form::open() }}
	<table>
		@foreach ($unidades as $unidade)
		<tr><td>{{ $unidade->id }}</td><td>{{ $unidade->sigla }}</td><td>{{ $unidade->descricao }}</td></tr>
		@endforeach
	</table>
	{{ Form::close() }}	
</div>
@stop