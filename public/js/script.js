function AcionaAtualizacao(pCampo, pParametros)
{
	var val_orig = pCampo.attr('data-valor-orig');
	if(pCampo.val() == '__/__/____'){
		pCampo.val('');
	}
	if(pCampo.val() != val_orig ){
		var parm = jQuery.parseJSON(pParametros);
		parm.nome_campo = pCampo.attr('id');
		parm.valor = pCampo.val();
		var url = '/paginas/carregueAjax.php?controller='+parm.controller+'&metodo='+parm.metodo;
		$.ajax({
			type: 'POST'
		  , url: url
		  , data: parm
		  , success:
				function(data) 
				{
					if(data.msg.tipoMsg == 'sucess'){
						pCampo.animate({backgroundColor: "#8de580"}, 250 );
						pCampo.animate({backgroundColor: "transparent"}, 750 );
						pCampo.attr('data-valor-orig', pCampo.val());
					}
					else{
						if(data.msg.tipoMsg == 'nop'){
							$.noty.closeAll();
							data.msg.textoMsg = 'Não foi possível atualizar devido ao seguinte erro: '+data.msg.textoMsg;
							var xx= noty({text: data.msg.textoMsg, timeout: timeout, type: data.msg.tipoMsg, layout:'topCenter' });
							ZeraMensagens();						
						}
					}
				}
		  , dataType: 'json'
		});
	}
}

function AcionaProcesso(pLink)
{
	var pParametros = $(pLink).attr('data-parameters');
	var parm = jQuery.parseJSON(pParametros);
	var url = parm.url;
	var td = $(pLink).parent();
	$.ajax({
		type: 'POST'
	  , url: url
	  , data: parm
	  , success:
			function(data) 
			{
				if(data.msg.tipoMsg == 'success'){
					// td.animate({backgroundColor: "#8de580"}, 250).delay(250).animate({backgroundColor: "transparent"}, 750 );
					if(data.novoConteudo){
						$(td).html(data.novoConteudo);
					}
					$(td).css('background-color', "#8de580");
					// td.animate({backgroundColor: "#8de580"}, 250).delay(250).animate({backgroundColor: "transparent"}, 750 );
					// td.animate({backgroundColor: "#8de580"}, 250);
				}
				else{
					if(data.msg.tipoMsg == 'nop'){
						$.noty.closeAll();
						data.msg.textoMsg = 'Não foi possível atualizar devido ao seguinte erro: '+data.msg.textoMsg;
						var xx= noty({text: data.msg.textoMsg, timeout: timeout, type: data.msg.tipoMsg, layout:'topCenter' });
						ZeraMensagens();						
					}
				}
			}
	  , dataType: 'json'
	});
}
