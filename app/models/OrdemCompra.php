<?php
/**
 * @property int $id Chave
 * @property string $numero Número da ordem de compra em código
 * @property int $fornecedor_id Fornecedor
 * @property date $data_emissao Data de emissão
 * @property date $data_base Data base 
 */
class OrdemCompra extends Eloquent{
	protected $table = 'ordens_compra';

	public function fornecedor()
	{
		return $this->belongsTo('Fornecedor');
	}
	
	public function ItensComprados()
	{
		return $this->belongsToMany('Item', 'itens_comprados', 'item_id', 'ordem_compra_id');
	}
	/**
	 * Cadastra uma nova ou recupera se houver. 
	 * Utilizado nas importacoes
	 * @param string $pdescricao Descricao da area que se deseja cadastrar.
	 * @return Area Retorna uma instancia de Area
	 */
	public static function Valido($pnumero, $pfornecedor_id = '', $pdata_emissao = '', $pdata_base = '')
	{
		$ordem = OrdemCompra::where('numero', '=', $pnumero)->first();
		if(!isset($ordem)){
			$ordem = new OrdemCompra();
			$ordem->numero = $pnumero;
			if($pfornecedor_id != ''){
				$ordem->fornecedor_id = $pfornecedor_id;				
			}
			$ordem->data_emissao = $pdata_emissao;
			$ordem->data_base = $pdata_base;
			$ordem->save();
		}
		return $ordem;
	}
	
	public function AdicionarItem($pitem, $pqtd, $pvalor)
	{
		$item_comprado = DB::table('itens_comprados')
				->where('item_id', '=', $pitem->id)
				->where('ordem_compra_id', '=', $this->id)
				->first();

		if(!isset($item_comprado)){
			DB::table('itens_comprados')->insert(array(
				'item_id' => $pitem->id,
				'ordem_compra_id'	=> $this->id,
				'qtd'  => $pqtd,
				'valor'  => $pvalor
			));			
		}
		
	}
	
	public function Cursor(){

	}
	
}