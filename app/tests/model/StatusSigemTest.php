<?php

class StatusSigemTest extends TestCase {

	public function testValido()
	{
		$texto = "novo status";
		$status = StatusSigem::Valido($texto);
		$status2 = StatusSigem::Valido($texto);
		
		$this->assertEquals($status->id, $status2->id);
	}
	
	public function testPregReplace()
	{
		$texto = "Liberado para Construção ";
		$x = preg_replace("/[^a-zA-Zçáãàéêõóíú\s]/", "", $texto);
		print $x;
	}
	
	public function tearDown()
	{
		parent::tearDown();
		$affectedRows = StatusSigem::where('descricao', '=', 'novo status')->delete();
				
	}
}