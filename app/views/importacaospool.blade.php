@extends('layout')

@section('conteudo')
<div class="interno">
		<div id="header">
			<h1>Importações de spool</h1>
			<div class="acoes"></div>
		</div>
	<div class="grid">
		{{ Form::open(array('url' => 'importacaospool', 'files' => true)) }}
		<div class="filtro">
			<div class="row">
				<p class="label slot-0" >Tipo:</p>
				<p class="slot-1-3">{{ Form::select('tipo', array('4' => 'TPSPOOL Excel', '5' => 'MATSPOOL Excel', '3' => 'Control Tub') , Input::old('tipo')) }}</p>
			</div>
			<div class="row">
				<p class="label slot-0">Arquivo a ser importado:</p>
				<p class="slot-1-3">{{ Form::file('arquivo',array('id'=> '0')); }}</p>
			</div>
		</div>
		<div style="text-align: center;margin: auto;">
			{{ Form::submit('Enviar'); }}
		</div>
		{{ Form::close() }}	
	</div>	
	@if (Session::has('msg'))
		<span class="error">{{ Session::get('msg') }}</span>
	@endif	
	<table class="listagem">
		<colgroup>
			<col width="70px">
			<col width="250px">
			<col width="100px">
			<col width="130px">
			<col width="auto">
			<col width="auto">
		</colgroup>
		<thead>
			<tr>
				<th class="c">Ação</th>
				<th>Arquivo</th>
				<th>Tipo</th>
				<th>Carregado em</th>
				<th>Importado em</th>
				<th>Resultado</th>
			</tr>
		</thead>
		<tbody>
			@foreach ($importacoes as $importacao)
			<tr>
				<td class="c"><a href="importacaospool/importe/{{ $importacao->id }}"><i class="icon-magic"></i></a> </td>
				<td>
					@if ($importacao->get_ArquivoExiste())
					<a href="/importacaospool/arquivo/{{ $importacao->id }}" target="_blank" title="Clique para obter esse arquivo">{{ $importacao->nome_arquivo }}</a>
					@else
					<span style="text-decoration: line-through; ">{{ $importacao->nome_arquivo }}</span>
					@endif
				</td>
				<td>@if ($importacao->tipo == 1 || $importacao->tipo == 4)
						TPSPOOL
					@elseif ($importacao->tipo == 2 || $importacao->tipo == 5)
						MATSPOOL
					@else
						ControlTub
					@endif
				</td>
				<td>{{ $importacao->created_at }}</td>
				<td>{{ $importacao->data_importacao }}</td>
				<td>{{ $importacao->resultado }}</td>
			</tr>
			@endforeach
		</tbody>
	</table>
	
	</div>
</div>
@stop