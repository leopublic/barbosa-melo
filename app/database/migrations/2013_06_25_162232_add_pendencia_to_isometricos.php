<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddPendenciaToIsometricos extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('isometricos', function(Blueprint $table) {
			$table->string('pendencia', 1000);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('isometricos', function(Blueprint $table) {
			$table->dropColumn('pendencia');
        });
    }

}
