<?php

use Illuminate\Database\Migrations\Migration;

class CreateTableIsometricos extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('isometricos', function($table)
		{
			$table->engine = 'InnoDB';
			$table->increments('id');
			$table->string('codigo',30);
			$table->timestamps();
		});			
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('isometricos');
	}

}