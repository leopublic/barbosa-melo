<?php

use Illuminate\Database\Migrations\Migration;

class AlterTableImportacoesSpool extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('importacoes_spool', function($table)
		{
			$table->integer('tipo')->nullable();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
	}

}