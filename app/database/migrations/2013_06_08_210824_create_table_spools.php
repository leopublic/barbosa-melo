<?php
use Illuminate\Database\Migrations\Migration;

class CreateTableSpools extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('spools', function($table)
		{
			$table->engine = 'InnoDB';
			$table->increments('id');
			$table->integer('linha_id')->unsigned()->nullable();
			$table->string('codigo',30);
			$table->timestamps();
		});			

		Schema::table('spools', function($table)
		{
			$table->foreign('linha_id')->references('id')->on('linhas');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('spools');
	}
}