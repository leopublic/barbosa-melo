<?php
/**
 * @property int $id Chave
 * @property string $descricao Descricao
 */
class Area extends Eloquent{
	protected $table = 'areas';

	/**
	 * Cadastra uma nova ou recupera se houver. 
	 * Utilizado nas importacoes
	 * @param string $pdescricao Descricao da area que se deseja cadastrar.
	 * @return Area Retorna uma instancia de Area
	 */
	public static function Valido($pdescricao)
	{
		$area = Area::where('descricao', '=', $pdescricao)->first();
		if(!isset($area)){
			$area = new Area();
			$area->descricao = $pdescricao;
			$area->save();
		}
		return $area;
	}
	
}