<?php
/**
 * Controla a verificação de acessos
 */
class AuthController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function getIndex()
	{
	}

	public function getLogin()
	{
		return View::make('login');
	}

	public function postLogin()
	{
		$username = Input::get('username');
		$password = Input::get('password');
		if(Auth::attempt(array('username' => $username, 'password'=> $password), true)){
			return Redirect::to('/');
		}
		else{
			return Redirect::to('auth/login')->with('login_errors', true);
		}
	}
	
	
	
}