<?php
/**
 * @property int $id Chave
 * @property string $codigo Codigo
 * @property string $descricao Descricao
 */
class UnidadeProd extends Eloquent{
	protected $table = 'unidades_prod';
	/**
	 * Cadastra um novo ou recupera se houver. 
	 * Utilizado nas importacoes
	 * @param string $pdescricao Descricao da area que se deseja cadastrar.
	 * @return UnidadeProd Retorna uma instancia de UnidadeProd
	 */
	public static function Valido($pcodigo, $pdescricao = '')
	{
		$pcodigo = preg_replace("/[^0-9]/","", $pcodigo);
		if($pcodigo != ''){
			$obj = UnidadeProd::where('codigo', '=', $pcodigo)->first();			
			if(!isset($obj)){
				$obj = new UnidadeProd();
				$obj->codigo = $pcodigo;
				$obj->descricao = $pdescricao;
				$obj->save();
			}
			else{
				if($pdescricao != ''){
					$obj->descricao = $pdescricao;
					$obj->save();									
				}
			}
		}
		else{
			$obj = new UnidadeProd();
		}
		return $obj;
	}	
}