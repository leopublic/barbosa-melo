<?php
/**
 * @property int $id Chave
 * @property string $descricao Descricao
 */
class Unidade extends Eloquent{
	protected $table = 'unidades';
	/**
	 * Cadastra uma nova ou recupera se houver. 
	 * Utilizado nas importacoes
	 * @param string $pdescricao Descricao da classe que se deseja cadastrar.
	 * @return Area Retorna uma instancia da classe
	 */
	public static function Valido($pdescricao)
	{
		$obj = Unidade::where('descricao', '=', $pdescricao)->first();
		if(!isset($obj)){
			$obj = new Unidade();
			$obj->descricao = $pdescricao;
			$obj->save();
		}
		return $obj;
	}

}