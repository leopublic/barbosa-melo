@extends('layout')

@section('conteudo')
<div class="interno">
	<h1>Linhas</h1>
	{{ Form::open() }}
	<table class="listagem">
		<thead>
			<tr>
				<th>Linha</th>
				<th>Item</th>
				<th>Descrição</th>
				<th class="r">Qtd necessária</th>
				<th class="r">Qtd req.</th>
				<th class="r">Qtd comprada</th>
				<th class="r">Qtd disp.</th>
			</tr>			
		</thead>
		<tbody>
			@foreach ($spools as $spool)
			<tr>
				<td class="iso" rowspan="{{ count($spool->ItensDoSpool()) }}">{{ $spool->codigo }}</td>
				@foreach ($spool->ItensDoSpool() as $item)
					<td>{{ $item->codigo }}</td>
					<td>{{ $item->descricao }}</td>
					<td class="r">{{ $item->qtd }}</td>
					<td class="r">{{ $item->qtd_solicitada }}</td>
					<td class="r">{{ $item->qtd_comprada }}</td>
					<td class="r">{{ $item->qtd_recebida }}</td>
				</tr>
				@endforeach			
			@endforeach			
		</tbody>
	</table>
	{{ Form::close() }}	
</div>
@stop