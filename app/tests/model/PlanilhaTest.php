<?php

class PlanilhaTest extends TestCase {
	public function testcolDt()
	{
		$plan = new Planilha();
		$plan->set_regAtualTxt('U5135	U5135	Sistema Combustíveis Gasosos	"2""-AD-5135-0036-CF-NI"	5135-AD-0036-001	2,286	0,431	20,00	"2"""	60	3,9	AC	2	NI	0		20/06/13	20/06/13	26/06/13	08/07/13	08/07/13	08/07/13	013921	0035	21/06/13							32 - AGUARD.JATO/PINT.FUNDO				UT1013.0001.01	5135-UT-1013.0001.01	IC-5400.00-5135-200-BDS-017	CBMSA	3	3	3	3	3	0	0	0	0	100	100	100	20	20	20');
		$plan->ExplodeColunas("\t");
		
		$this->assertEquals('2013-06-20', $plan->colDt(17, 'd/m/y', 'Y-m-d'));
		$this->assertEquals('2013-06-26', $plan->colDt(18,'d/m/y', 'Y-m-d'));
		$this->assertEquals('2013-07-08', $plan->colDt(19, 'd/m/y', 'Y-m-d'));

		$plan = new Planilha();
		$plan->set_regAtualTxt('SE-5606	IS-5400.00-5606-200-BDS-501	"A "	"Liberado para Construção "	"20/08/2012 19:32:11 "	"/4""-AF-5606-0501-Bh-NI"	"E - PARA CONSTRUÇÃO"	187	2012/12/17	A	73,8702051	" "	" "	" "	" "	DE-5400.00-5606-947-BDS-503	0	');
		$plan->ExplodeColunas("\t");
		
		$this->assertEquals('2012-08-20 19:32:11', $plan->colDt(4, 'd/m/Y H:i:s', 'Y-m-d H:i:s'));

	}
	public function testcolLimpa()
	{
		$plan = new Planilha();
		$plan->set_regAtualTxt('"A&%$ "	"Liberado para Construção "');
		$plan->ExplodeColunas("\t");
		$x = $plan->col(1);
		print "\nLimpo=".substr($x,0, -2);
		print "\nCharacter =".ord(substr($plan->colLimpa(0),-1,1));
		
			// $this->assertEquals('A', $plan->colLimpa(0));
		
	}

	public function testIndiceColuna()
	{
		$plan = new Planilha();
		$caminho = "/dados/projetos/barbosa_melo/trunk/public/uploads/spo/2321";
		$plan->abrir($caminho, 12, 40);
		$ind = $plan->IndiceColuna("LINHA");
		$this->assertEquals(3, $ind);

		$ind = $plan->IndiceColuna("UNIDADE");
		$this->assertEquals(0, $ind);
		$ind = $plan->IndiceColuna("DESCRIÇÃO");
		$this->assertEquals(2, $ind);
		
	}
}