<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddUnidadeProdIdToIsometricos extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('isometricos', function(Blueprint $table) {
			$table->integer('unidade_prod_id')->unsigned()->nullable();            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('isometricos', function(Blueprint $table) {
			$table->dropColumn('unidade_prod_id');            
        });
    }

}
