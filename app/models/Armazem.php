<?php
/**
 * @property int $id Chave
 * @property string $descricao Descricao
 * @property int $local_id Local onde está o armazem
 */
class Armazem extends Eloquent{
	protected $table = 'armazens';
	/**
	 * Cadastra uma nova ou recupera se houver. 
	 * Utilizado nas importacoes
	 * @param string $pdescricao Descricao da classe que se deseja cadastrar.
	 * @return Area Retorna uma instancia da classe
	 */
	public static function Valido($pdescricao, $plocal_id)
	{
		$obj = Armazem::where('descricao', '=', $pdescricao)->first();
		if(!isset($obj)){
			$obj = new Armazem();
			$obj->descricao = $pdescricao;
			$obj->local_id = $plocal_id;
			$obj->save();
		}
		return $obj;
	}

}