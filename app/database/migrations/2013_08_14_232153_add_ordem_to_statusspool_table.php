<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddOrdemToStatusspoolTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('status_spool', function(Blueprint $table) {
            $table->integer("ordem")->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('status_spool', function(Blueprint $table) {
            $table->dropColumn('ordem');            
        });
    }

}
