<?php

class UnidadeProdTest extends TestCase {
	public function testValido()
	{
		$uni1 = UnidadeProd::Valido('U-5444 (ssdsdj)', 'descricao aaa');
		$uni2 = UnidadeProd::Valido('U5444');
		$uni3 = UnidadeProd::Valido('U-5444');
		$this->assertEquals($uni1->id, $uni2->id);
		$this->assertEquals($uni3->id, $uni2->id);
	}
	public function testValido2()
	{
		$uni3 = UnidadeProd::Valido('U-6444');
		$uni2 = UnidadeProd::Valido('U6444');
		$uni1 = UnidadeProd::Valido('U-6444 (ssdsdj)', 'descricao bbb');
		$this->assertEquals($uni1->id, $uni2->id);
		$this->assertEquals($uni1->id, $uni3->id);
	}
	
}