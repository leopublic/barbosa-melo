<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddRevisaoToIsometricosTable extends Migration {
    public function up()
    {
        Schema::table('isometricos', function(Blueprint $table) {
			$table->string('revisao',3)->nullable();
			$table->dateTime('data_inclusao_controle')->nullable();
			$table->integer('status_sigem_id')->unsigned()->nullable();
			$table->integer('status_emissao_id')->unsigned()->nullable();
			$table->decimal('peso', 14, 7)->nullable();
			$table->string('grd',10)->nullable();
			$table->date('data_emissao')->nullable();			
        });
    }
    public function down()
    {
        Schema::table('isometricos', function(Blueprint $table) {
			$table->dropColumn('revisao');
			$table->dropColumn('data_inclusao_controle');
			$table->dropColumn('status_sigem_id');
			$table->dropColumn('status_emissao_id');
			$table->dropColumn('peso');
			$table->dropColumn('grd');
			$table->dropColumn('data_emissao');
        });		
    }
}