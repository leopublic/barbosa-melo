<?php
/**
 * @property int $id Chave
 * @property string $descricao Descricao
 * @property string $cgc CGC
 */
class Fornecedor extends Eloquent{
	protected $table = 'fornecedores';
	
	
	/**
	 * Cadastra uma nova ou recupera se houver. 
	 * Utilizado nas importacoes
	 * @param string $pdescricao Descricao da classe que se deseja cadastrar.
	 * @return Area Retorna uma instancia da classe
	 */
	public static function ValidoPeloCgc($pcgc, $pdescricao)
	{
		$obj = Fornecedor::where('cgc', '=', $pcgc)->first();
		if(!isset($obj)){
			$obj = new Fornecedor();
			$obj->cgc = $pcgc;
			$obj->descricao = $pdescricao;
			$obj->save();
		}
		return $obj;
	}
	/**
	 * Cadastra uma nova ou recupera se houver. 
	 * Utilizado nas importacoes
	 * @param string $pdescricao Descricao da classe que se deseja cadastrar.
	 * @return Area Retorna uma instancia da classe
	 */
	public static function ValidoPelaDescricao($pcgc, $pdescricao)
	{
		$obj = Fornecedor::where('descricao', '=', $pdescricao)->first();
		if(!isset($obj)){
			$obj = new Fornecedor();
			$obj->cgc = $pcgc;
			$obj->descricao = $pdescricao;
			$obj->save();
		}
		return $obj;
	}

}