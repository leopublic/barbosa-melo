<?php
class Planilha{
	protected $arquivo;
	protected $regAtualTxt;
	protected $regAtualArray;
	protected $cabecalhoArray;
	protected $qtdLidas;
	protected $qtdColunas;
	protected $sep;

	public function abrir($pcaminho, $pqtdLinhasHeader, $pqtdColunas, $psep = "\t")
	{
		$this->arquivo = fopen($pcaminho, "r");
		$this->qtdLidas = 0;
		$this->qtdColunas = $pqtdColunas;
		$this->sep = $psep;
		$this->obterProximo();
		//Posiciona na primeira linha valida
		while (!feof($this->arquivo) && $this->qtdLidas < $pqtdLinhasHeader) {
			$this->obterProximo();
		}
		$this->cabecalhoArray = explode($psep, $this->regAtualTxt);		
		$this->obterProximo();
		$this->qtdLidas = 0;
	}
	
	public function eof()
	{
		return feof($this->arquivo);
	}
	
	public function indiceColuna($ptituloColuna)
	{
		return array_search($ptituloColuna, $this->cabecalhoArray);
	}

	public function obterProximo($pincrementa = true)
	{
		$this->regAtualTxt = utf8_encode(fgets($this->arquivo));
		while (!feof($this->arquivo) && substr_count($this->regAtualTxt,"\t") <  $this->qtdColunas - 1) {
			$this->regAtualTxt .= utf8_encode(fgets($this->arquivo));
		}
		$this->ExplodeColunas($this->sep);
		if($pincrementa){
			$this->qtdLidas++;
		}
	}

	public function ExplodeColunas($psep){
		$this->regAtualArray = explode($psep, $this->regAtualTxt);		
	}
	
	public function get_qtdLinhas()
	{
		return $this->qtdLidas;
	}
	
	public function get_regAtualTxt()
	{
		return $this->regAtualTxt;
	}
	
	public function set_regAtualTxt($pval)
	{
		$this->regAtualTxt = $pval;
	}

	public function colDt($i, $pformatIn, $pformatOut = 'Y-m-d H:i:s')
	{
		if($pformatIn == 'd/m/y' || $pformatIn == 'y-m-d'){
			$tam = 8;
		}
		elseif($pformatIn == 'd/m/Y' || $pformatIn == 'Y-m-d'){
			$tam = 8;
		}
		elseif($pformatIn == 'Y-m-d H:i:s' || $pformatIn == 'd/m/Y H:i:s'){
			$tam = 19;
		}

		$x = substr($this->col($i), 0, $tam);
		$date = DateTime::createFromFormat($pformatIn, $x);
		if(is_object($date)){
			$ret = $date->format($pformatOut);				
		}
		else{
			$ret = '';
		}
		return $ret;
	}

	public function colLimpa($i)
	{
		$x = preg_replace('#[^([:alpha:]|[:digit:]|\160|\194)]#', '', $this->col($i));
		return $x;
	}
	
	public function col($i)
	{
		$valor = trim($this->regAtualArray[$i]);
		// if($i == count($this->regAtualArray) - 1){
		// 	$valor = substr($valor, 0, -1);
		// }
		if(substr($valor,0,1) == '"' && substr($valor, -1) == '"'){
			$valor = substr($valor,1,-1);
		}
		return $valor;
	}
	
	public function feche()
	{
		fclose($this->arquivo);		
	}
}