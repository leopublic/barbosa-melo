@extends('layout')

@section('conteudo')
<div class="interno">
	{{ Form::open(array("name" => "form", "id" => "form")) }}
	<div class="grid">
		<div class="row">
			<h1 class="slot-0-1-2-3">Conferência de Importações</h1>
			<p class="slot-4-5" style="margin: 0; text-align:right;">
				<i class="icon-search" style="background-color: #00b0ca; padding-left:25px; padding-right:25px; padding-top:5px; padding-bottom:5px; color:#fff; cursor:pointer;" onclick="$('#form').submit();" title="Clique para aplicar os filtros"></i>
			</p>

		</div>
	</div>	
	<div class="grid filtro">
		<div class="row">
			<p class="slot-0-1" style="margin-top:20px;">Status SEI:<br/>{{ Form::select('status_emissao_id', $status_emissao, Input::old('status_emissao_id')) }}</p>
			<p class="slot-2-3" style="margin-top:20px;">Visual solda (ControlTub):<br/>{{ Form::select('visual_solda', array('' => '(todos)','sim' => 'preenchida', 'não' => 'em branco') , Input::old('visual_solda')) }}</p>
			<p class="slot-4-5" style="margin-top:20px;">Unidade:<br/>{{ Form::select('unidade_prod_id',  array('' => '(todos)') + $unidades_prod , Input::old('unidade_prod_id')) }}</p>
		</div>
		<div class="row">
			<p class="slot-0-1" style="margin-top:20px;">Sem filtro na col. Q:<br/>{{ Form::checkbox('col_q', '1', Input::old('col_q')) }}</p>
			<p class="slot-2-3" style="margin-top:20px;">Sem filtro na col R:<br/>{{ Form::checkbox('col_r', '1', Input::old('col_r')) }}</p>
			<p class="slot-4-5" style="margin-top:20px;">Com pendência:<br/>{{ Form::checkbox('pendencia', '1', Input::old('pendencia')) }}</p>
		</div>
		<div class="row">
			<p class="slot-0-1" style="margin-top:20px;">TPSpool:<br/>{{ Form::select('ok_tpspool', array(''=> '(todos)', '1'=> 'só ok', '0'=> 'só não ok'), Input::old('ok_tpspool')) }}</p>
			<p class="slot-2-3" style="margin-top:20px;">MATSpool:<br/>{{ Form::select('ok_matspool', array(''=> '(todos)', '1'=> 'só ok', '0'=> 'só não ok'), Input::old('ok_matspool')) }}</p>
			<p class="slot-4-5" style="margin-top:20px;">Control Tub:<br/>{{ Form::select('ok_controltub', array(''=> '(todos)', '1'=> 'só ok', '0'=> 'só não ok'), Input::old('ok_controltub')) }}</p>
		</div>
	</div>		
	<? $total = 0;
	$iso_ant = "";
	 ?>
	<table class="listagem">
		<colgroup>
			<col width="250px">
			<col width="180px">
			<col width="180px">
			<col width="100px">
			<col width="100px">
			<col width="100px">
			<col width="100px">
			<col width="130px">
		</colgroup>
		<thead>
		<tr>
			<th style="text-align:left;">Isométrico</th>
			<th style="text-align:left;">Linha</th>
			<th style="text-align:left;">Spool</th>
			<th style="text-align:center;">Status</th>
			<th style="text-align:center;">TPSpool</th>
			<th style="text-align:center;">MatSpool</th>
			<th style="text-align:center;">ControlTub</th>
			<th style="text-align:left;">Visual de solda</th>
			<th class="r">
				Peso SEI(kg)
				@if (isset($peso_total_sei)) 
					<br/>{{ $peso_total_sei }}
				@endif 
			</th>
			<th class="r">
				Peso(kg)
				@if (isset($peso_total)) 
					<br/>{{ $peso_total }}
				@endif 
			</th>
		</tr>			
		</thead>	
		<tbody>
		@foreach ($spools as $spool)
		<? 
		if ($iso_ant == "" || $iso_ant != $spool->codigo_isometrico){
			$iso_ant = $spool->codigo_isometrico;
			$total += $spool->peso_sei; 
		}

		?>
		<tr style="border-top: solid 2px #777;">
			<td style="text-align:left;">{{ $spool->codigo_isometrico }}</td>
			<td style="text-align:left;">{{ $spool->codigo_linha }}</td>
			<td style="text-align:left;">{{ $spool->codigo_spool }}</td>
			<td style="text-align:center;">{{ $spool->descricao_emissao }}</td>
			<td style="text-align:center;">
				@if($spool->ok_tpspool==1)
					<i class="icon-ok"></i>
				@else
					<i class="icon-remove" style="color:red;"></i>
				@endif
			</td>
			<td style="text-align:center;">
				@if($spool->ok_matspool==1)
					<i class="icon-ok"></i>
				@else
					<i class="icon-remove" style="color:red;"></i>
				@endif
			</td>
			<td style="text-align:center;">
				@if($spool->ok_controltub==1)
					<i class="icon-ok"></i>
				@else
					<i class="icon-remove" style="color:red;"></i>
				@endif
			</td>
			<td style="text-align:left;">{{ $spool->visual_solda }}</td>
			<td class="r">{{ number_format($spool->peso_sei, 2, ",", ".") }}</td>
			<td class="r">{{ number_format($spool->peso, 2, ",", ".") }}</td>
		</tr>
		@endforeach			
			
		</tbody>
	</table>
	<? print "<br> Total calculado PHP: ".$total; ?>
	{{ Form::close() }}	
</div>
@stop