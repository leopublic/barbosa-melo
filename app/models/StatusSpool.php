<?php
/**
 * @property int $id Chave
 * @property string $descricao Descricao
 */
class StatusSpool extends Eloquent {
	protected $table = 'status_spool';
    protected $guarded = array();

    public static $rules = array();
}