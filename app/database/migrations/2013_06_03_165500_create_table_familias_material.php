<?php

use Illuminate\Database\Migrations\Migration;

class CreateTableFamiliasMaterial extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('familias_material', function($table)
		{
			$table->engine = 'InnoDB';
			$table->increments('id');
			$table->string('descricao');
			$table->timestamps();
		});	
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('familias_material');
	}

}