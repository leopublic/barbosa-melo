@extends('layout')

@section('conteudo')
<div class="interno">
	{{ Form::open(array("name" => "form", "id" => "form")) }}
	<div class="grid">
		<div class="row">
			<h1 class="slot-0-1-2-3-4-5">Situação geral</h1>
		</div>
		<div class="row" style="margin-top:10px;">
			<div class="nivel1">
				<div><p class="valor" style="float:right;">{{ $para_construcao }}</p>E - PARA CONSTRUÇÃO</div>
				<div class="nivel2"><p class="valor" style="float:right;">{{ $total[7] }}</p>Não iniciados</div> 
				<div class="nivel2"><p class="valor" style="float:right;">{{ $total[8] }}</p>Reservado</div> 
				<div class="nivel2"><p class="valor" style="float:right;">{{ $total[2] }}</p>Em fabricação</div> 
				<div class="nivel2"><p class="valor" style="float:right;">{{ $total[2] }}</p>Fabricado
					<div class="nivel3"><p class="valor" style="float:right;">{{ $total[21] }}</p>Aguardando..</div> 
					<div class="nivel3"><p class="valor" style="float:right;">{{ $total[22] }}</p>Pronto para montagem</div> 
				</div> 
			</div>
		</div>
	</div>	
</div>
@stop