<?php

class StatusControltub extends Eloquent {
	protected $table = 'status_controltub';
    protected $guarded = array();

    public static $rules = array();
	/**
	 * Cadastra um novo ou recupera se houver. 
	 * Utilizado nas importacoes
	 * @param string $pdescricao Descricao da area que se deseja cadastrar.
	 * @return UnidadeProd Retorna uma instancia de UnidadeProd
	 */
	public static function Valido($pdescricao)
	{
		if(trim($pdescricao) == '' || trim($pdescricao) == '-'){
			$obj = new StatusControltub();
		}
		else{
			$obj = StatusControltub::where('descricao', '=', $pdescricao)->first();
			if(!isset($obj)){
				$obj = new StatusControltub();
				$obj->descricao = $pdescricao;
				$obj->save();
			}
		}
		return $obj;
	}	
	
}