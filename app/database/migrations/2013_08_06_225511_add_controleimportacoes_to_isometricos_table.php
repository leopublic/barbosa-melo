<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddControleImportacoesToIsometricosTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('isometricos', function(Blueprint $table) {
            $table->tinyInteger('ok_matspool')->unsigned()->null();
            $table->tinyInteger('ok_tpspool')->unsigned()->null();
            $table->tinyInteger('ok_controltub')->unsigned()->null();            
            $table->tinyInteger('ok_sei')->unsigned()->null();            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('isometricos', function(Blueprint $table) {
            $table->dropColumn('ok_matspool');
            $table->dropColumn('ok_tpspool');
            $table->dropColumn('ok_controltub');            
            $table->dropColumn('ok_sei');            
        });
    }

}
