<?php

use Illuminate\Database\Migrations\Migration;

class CreateTableItensComprados extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('itens_comprados', function($table)
		{
			$table->engine = 'InnoDB';
			$table->increments('id');
			$table->integer('item_id')->unsigned();
			$table->integer('ordem_compra_id')->unsigned();
			$table->integer('ordem')->unsigned()->nullable();
			$table->decimal('qtd', 11,2)->nullable();
			$table->decimal('valor', 11,2)->nullable();
			$table->timestamps();
		});	
		
		Schema::table('itens_comprados', function($table)
		{
			$table->foreign('item_id')->references('id')->on('itens');
			$table->foreign('ordem_compra_id')->references('id')->on('ordens_compra');
		});	
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('itens_comprados');
	}

}