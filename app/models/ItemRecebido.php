<?php
class ItemRecebido extends Eloquent{
	protected $table = 'itens_recebidos';
	
	public function Item(){
		return $this->belongsTo('Item');
	}
	
	public function Recebimento(){
		return $this->belongsTo('Recebimento');
	}
	
}