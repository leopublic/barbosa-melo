<?php

class OrdemCompraController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function getIndex()
	{
		$ordens= OrdemCompra::all();
		return View::make('ordemcompra')->with('ordens', $ordens);
	}

	public function getReceber($id)
	{
		$ordem = OrdemCompra::find($id);
		$itens_comprados = ItemComprado::where('ordem_compra_id', '=', $id)->with('item')->get();
		return View::make('ordemcompra_receber')
					->with('ordem', $ordem)
					->with('itenscomprados', $itens_comprados);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		//
	}
	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}
	
	public function getImporte($id){
		$caminho = "../public/uploads/oc/".$id;
		if (file_exists($caminho)) {
			$handle = fopen($caminho, "r");
			$linha = 1;
			$buffer = fgets($handle);
			$buffer = utf8_encode(fgets($handle));
			$codigo_ordem = '';
			while (!feof($handle) || strlen($buffer) >10) {
				if($linha > 10 && trim(str_replace("\n", "", str_replace("\t", "", $buffer))) != ''){
					if(substr_count($buffer,"\t") < 16){
						while(substr_count($buffer,"\t") < 16){
							$buffer .= utf8_encode(fgets($handle));
							$linha++;
						}
					}
					$colunas = explode("\t", $buffer);
					$fornecedor = Fornecedor::ValidoPelaDescricao("", $colunas[0]);
					$unidade = Unidade::Valido($colunas[16]);
//					$familia = FamiliaMaterial::Valido($colunas[9]);
					
					$item = Item::Valido($colunas[15], $colunas[14], $unidade->id, null );

					if($codigo_ordem != $colunas[1]){
						$codigo_ordem = $colunas[1];
						$data_emissao = '20' . substr($colunas[3],6,2) . '-' . substr($colunas[3],3,2) . '-' . substr($colunas[3],0,2);
						$data_base = '20' . substr($colunas[4],6,2) . '-' . substr($colunas[4],3,2) . '-' . substr($colunas[4],0,2);
						$ordem_compra = OrdemCompra::Valido($colunas[1], $fornecedor->id, $data_emissao, $data_base);					
					}
					$qtd = str_replace(",", ".", $colunas[18]);
					$valor = str_replace(",", ".", $colunas[11]);
					$ordem_compra->AdicionarItem($item, $qtd, $valor);

				}
				$buffer = utf8_encode(fgets($handle));
				$linha++;
			}
			fclose($handle);
		}
		else{
			print 'Arquivo '.$caminho.' não encontrado';
		}

	}
}