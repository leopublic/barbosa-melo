<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddColqToIsometricosTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('isometricos', function(Blueprint $table) {
            $table->string('col_q', 200)->nullable();
            $table->string('col_r', 200)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('isometricos', function(Blueprint $table) {
            $table->dropColumn('col_q');
            $table->dropColumn('col_r');            
        });
    }

}
