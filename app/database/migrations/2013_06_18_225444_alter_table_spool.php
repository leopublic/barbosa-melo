<?php

use Illuminate\Database\Migrations\Migration;

class AlterTableSpool extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('spools', function($table)
		{
			$table->string('revisao')->nullable();
			$table->decimal('comprimento',10,2)->nullable();
			$table->decimal('diametro_1',10,2)->nullable();
			$table->decimal('espessura',10,2)->nullable();
			$table->decimal('peso',10,2)->nullable();
			$table->string('cond_pintura')->nullable();
		});
//
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
	}

}