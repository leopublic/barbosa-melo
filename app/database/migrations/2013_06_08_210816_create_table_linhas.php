<?php

use Illuminate\Database\Migrations\Migration;

class CreateTableLinhas extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('linhas', function($table)
		{
			$table->engine = 'InnoDB';
			$table->increments('id');
			$table->integer('isometrico_id')->unsigned()->nullable();
			$table->string('codigo',30);
			$table->timestamps();
		});			

		Schema::table('linhas', function($table)
		{
			$table->foreign('isometrico_id')->references('id')->on('isometricos');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('Linhas');
	}

}