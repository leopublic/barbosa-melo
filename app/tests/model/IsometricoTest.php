<?php

class IsometricoTest extends TestCase {
	public function testCodigoPadrao()
	{
		$x = 'IC-5400.00-5135-200-BDS-017';
		$codigo = Isometrico::CodigoPadrao($x);
		$this->assertEquals($x, $codigo);

		$x = 'IS-5400.00-6825-200-BDS-237';
		$codigo = Isometrico::CodigoPadrao($x);
		$this->assertEquals('IC-5400.00-6825-200-BDS-237', $codigo);
		
	}
	
}