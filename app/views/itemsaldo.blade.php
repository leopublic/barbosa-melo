@extends('layout')

@section('conteudo')
<div class="interno">
	<h1>Saldo em estoque de materiais</h1>
	{{ Form::open() }}	
	<div class="filtro">
		<ul>
			<li>
				<p>Código:</p>{{ Form::text('codigo', Input::old('codigo')) }}
			</li>
			<li>
				<p>Descricão:</p>{{ Form::text('descricao', Input::old('descricao')) }}
			</li>
		</ul>	
		{{ Form::submit('Filtrar') }}
	</div>
	{{ Form::close() }}	
	{{ Form::open() }}
	<table class="listagem">
		<thead>
			<tr>
				<th>Código</th>
				<th>Descrição</th>
				<th class="r">Saldo</th>
				<th>Unidade</th>
			</tr>
		</thead>
		<tbody>
		@if (isset($itens))
			@foreach ($itens as $item)
			<tr>
				<td>{{ $item->codigo }}</td>
				<td>{{ $item->descricao }}</td>
				<td class="r">{{ $item->saldo }}</td>
				<td>{{ $item->unidade->descricao }}</td>
			</tr>
			@endforeach
		@endif
		</tbody>
	</table>
	{{ Form::close() }}	
</div>
@stop