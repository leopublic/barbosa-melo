<?php

class AreaController extends BaseController {

	public function getIndex()
	{
		$areas = Area::all();
		return View::make('cadastro.areas')->with('areas', $areas);
	}
}