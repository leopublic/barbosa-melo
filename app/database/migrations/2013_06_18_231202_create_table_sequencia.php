<?php
use Illuminate\Database\Migrations\Migration;
class CreateTableSequencia extends Migration {
	public function up()
	{
		Schema::create('sequencias', function($table) {
			$table->increments('id');
			$table->string('descricao');
			$table->timestamps();
		});
	}

	public function down()
	{
		Schema::drop('sequencias');
	}
}