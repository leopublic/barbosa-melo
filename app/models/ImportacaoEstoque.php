<?php
class ImportacaoEstoque extends Importacao{
	protected $table = 'importacoes_estoque';

	public static function get_subdiretorio()
	{
		return "estoque";
	}
	
	public function Importar()
	{
		$plan = new Planilha();
		$plan->abrir($this->get_caminhoArquivo(), 9, 28);
		while(!$plan->eof()){
			// Processa registro
			$item = Item::Valido($plan->col(6), $plan->col(5), null, null);
			$saldo = str_replace(",", ".", $plan->col(28));
			if ($saldo == ''){
				$saldo = 0;
			}
			$item->saldo = $saldo;
			$item->save();
			// Le próximo
			$plan->obterProximo();
		}
		$this->data_importacao = date('Y-m-d H:i:s');
		$this->save();		
	}

}