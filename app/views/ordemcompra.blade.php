@extends('layout')

@section('conteudo')
<div class="interno">
	<h1>Ordens de compra</h1>
	{{ Form::open() }}
	<table class="listagem">
		<thead>
			<tr>
				<th>Ação</th>
				<th>Fornecedor</th>
				<th>Número</th>
				<th>Data de emissão</th>
			</tr>
		</thead>
		<tbody>
			@foreach ($ordens as $ordem)
			<tr>
				<td><a href="{{ URL::to('ordemcompra/receber/') }}/{{ $ordem->id }}">Receber</a></td>
				<td>{{ $ordem->fornecedor->descricao }}</td>
				<td>{{ $ordem->numero }}</td>
				<td>{{ $ordem->data_emissao }}</td>
			</tr>
			@endforeach			
		</tbody>
	</table>
	{{ Form::close() }}	
</div>
@stop