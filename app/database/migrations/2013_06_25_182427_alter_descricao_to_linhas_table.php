<?php

use Illuminate\Database\Migrations\Migration;

class AlterDescricaoToLinhasTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
		DB::raw('alter table linhas modify codigo varchar(1000)');
	}

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
    }

}
