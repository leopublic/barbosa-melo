@extends('layout')

@section('conteudo')
<div class="interno">
	<h1>Recebimentos</h1>
	{{ Form::open() }}
	<table class="listagem">
		<thead>
			<tr>
				<th>Ordem de compra</th>
				<th>Data recebimento</th>
				<th>Armazém</th>
				<th>Código</th>
				<th>Item</th>
				<th class="r">Qtd recebida</th>
			</tr>
		</thead>
		<tbody>
			@foreach ($recebimentos as $recebimento)
			<tr>
				<td class ="iso" rowspan="{{ $recebimento->ItemRecebido->count() }}">{{ $recebimento->OrdemCompra->numero }}</td>
				<td class ="iso" rowspan="{{ $recebimento->ItemRecebido->count() }}">{{ $recebimento->data_recebimento }}</td>
				<td class ="iso" rowspan="{{ $recebimento->ItemRecebido->count() }}">{{ $recebimento->Armazem->descricao }}</td>
				@foreach ($recebimento->ItemRecebido as $itemrec)
						<td>{{ $itemrec->Item->codigo }}</td>
						<td>{{ $itemrec->Item->descricao }}</td>
						<td class="r">{{ $itemrec->qtd }}</td>
					</tr>
				@endforeach
			@endforeach
		</tbody>
	</table>
	{{ Form::close() }}	
</div>
@stop