@extends('layout')

@section('conteudo')
<div class="interno">
	<h1>Isométricos</h1>
	{{ Form::open() }}
	<table class="listagem">
		<colgroup>
			<col width="200px">
			<col width="100px">
			<col width="auto">
			<col width="80px">
			<col width="80px">
		</colgroup>
			
		<thead>
			<tr>
				<th>Isométrico</th>
				<th>Item</th>
				<th>Descrição</th>
				<th class="r">Qtd necessária</th>
				<th class="r">Saldo em estoque</th>
			</tr>			
		</thead>
		<tbody>
			@foreach ($isometricos as $iso)
			<tr style="border-top: solid 2px #777;">
				<td class="iso" colspan="{{ $iso->qtdItens }}">{{ $iso->codigo }}</td>
				@foreach ($iso->linha as $linha)
					@foreach ($linha->spool as $spool)
						@foreach ($spool->itemnecessario as $itemnec)
						<td>{{ $itemnec->item->codigo }}</td>
						<td>{{ $itemnec->item->descricao }}</td>
						<td>{{ $itemnec->qtd }}</td>
						<td>{{ $itemnec->item->saldo }}</td>
						</tr>
						@endforeach			
					@endforeach			
				@endforeach			
			@endforeach			
		</tbody>
	</table>
	{{ Form::close() }}	
</div>
@stop