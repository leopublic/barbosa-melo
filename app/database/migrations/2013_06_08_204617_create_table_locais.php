<?php

use Illuminate\Database\Migrations\Migration;

class CreateTableLocais extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('locais', function($table)
		{
			$table->engine = 'InnoDB';
			$table->increments('id');
			$table->string('descricao',500);
			$table->timestamps();
		});			
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('locais');
	}

}