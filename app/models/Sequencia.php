<?php
/**
 * @property int $id Chave
 * @property string $descricao Descricao
 */
class Sequencia extends Eloquent{
	protected $table = 'sequencias';
	/**
	 * Cadastra um novo ou recupera se houver. 
	 * Utilizado nas importacoes
	 * @param string $pdescricao Descricao da area que se deseja cadastrar.
	 * @return Material Retorna uma instancia de Material
	 */
	public static function Valido($pdescricao)
	{
		if($pdescricao == ''){
			$obj = new Sequencia();
		}
		else{
			$obj = Sequencia::where('descricao', '=', $pdescricao)->first();
			if(!isset($obj)){
				$obj = new Sequencia();
				$obj->descricao = $pdescricao;
				$obj->save();
			}
		}
		return $obj;
	}	
}