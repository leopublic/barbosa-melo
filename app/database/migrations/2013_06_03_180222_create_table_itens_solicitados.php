<?php

use Illuminate\Database\Migrations\Migration;

class CreateTableItensSolicitados extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('itens_solicitados', function($table)
		{
			$table->engine = 'InnoDB';
			$table->increments('id');
			$table->integer('item_id')->unsigned();
			$table->integer('requisicao_material_id')->unsigned();
			$table->integer('ordem')->unsigned()->nullable();
			$table->decimal('qtd', 11,2)->nullable();
			$table->timestamps();
		});	
		
		Schema::table('itens_solicitados', function($table)
		{
			$table->foreign('item_id')->references('id')->on('itens');
			$table->foreign('requisicao_material_id')->references('id')->on('requisicoes_material');
		});	
		
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('itens_solicitados');
	}

}