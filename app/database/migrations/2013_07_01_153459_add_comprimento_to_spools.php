<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddComprimentoToSpools extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('spools', function(Blueprint $table) {
            $table->decimal('comprimento', 10, 3)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('spools', function(Blueprint $table) {
            $table->dropColumn('comprimento');            
        });
    }

}
