<?php
use Illuminate\Database\Migrations\Migration;
class CreateTableImportacoesIsometrico extends Migration {
	public function up()
	{
		Schema::create('importacoes_isometrico', function($table) {
			$table->increments('id');
			$table->integer('usuario_id')->unsigned()->nullable();
			$table->string('nome_arquivo');
			$table->datetime('data_importacao');
			$table->integer('tipo');
			$table->timestamps();
		});
	}

	public function down()
	{
		Schema::drop('importacoes_isometrico');
	}

}