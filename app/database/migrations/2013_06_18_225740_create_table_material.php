<?php
use Illuminate\Database\Migrations\Migration;
class CreateTableMaterial extends Migration {
	public function up()
	{
		Schema::create('materiais', function($table) {
			$table->increments('id');
			$table->string('descricao');
			$table->timestamps();
		});
	}

	public function down()
	{
		Schema::drop('materiais');
	}

}