<?php

class IsometricoController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function getIndex()
	{
		$status_sigem = DB::table('status_sigem')
					->orderBy('descricao', 'asc')
					->lists('descricao','id')
			;
		$status_emissao = DB::table('status_emissao')
					->orderBy('descricao', 'asc')
					->lists('descricao','id')
			;
		return View::make('isometricoscontrole')
					->with('status_sigem', $status_sigem)
					->with('status_emissao', $status_emissao)
				;
	}
	
	public function postIndex()
	{
		Input::flash();

		$isometricos = DB::table('isometricos')
				;
		if(Input::get('status_sigem_id') != ''){
			$isometricos = $isometricos->where('status_sigem_id', '=', Input::get('status_sigem_id'));
		}
		if(Input::get('status_emissao_id') != ''){
			$isometricos = $isometricos->where('status_emissao_id', '=', Input::get('status_emissao_id'));
		}

		$rows = $isometricos->leftjoin('status_sigem', 'isometricos.status_sigem_id', '=', 'status_sigem.id')
					->leftjoin('status_emissao', 'isometricos.status_emissao_id', '=', 'status_emissao.id')
					->select('isometricos.*', 'status_sigem.descricao as descricaoSigem', 'status_emissao.descricao as descricaoEmissao')
					->get()
				;

		$status_sigem = DB::table('status_sigem')
					->orderBy('descricao', 'asc')
					->lists('descricao','id')
			;
		$status_emissao = DB::table('status_emissao')
					->orderBy('descricao', 'asc')
					->lists('descricao','id')
			;
		return View::make('isometricoscontrole')
					->with('isometricos', $rows)
					->with('status_sigem', $status_sigem)
					->with('status_emissao', $status_emissao)
				;
	}
	


	public function getMateriais()
	{
		$isometricos =  DB::table ('isometricos')
				->join('linhas', 'linhas.isometrico_id', '=', 'isometricos.id')
				->join('spools', 'spools.linha_id', '=', 'linhas.id')
				->join('itens_necessarios', 'itens_necessarios.spool_id', '=', 'spools.id')
				->join('itens', 'itens.id', '=', 'itens_necessarios.item_id')
				->get(array('isometricos.codigo as codigo_iso', 'itens.codigo as codigo_item', 'itens.descricao as descricao_item'))
				;

//		$isometricos = Isometrico::whereRaw('id in (select distinct isometrico_id from linhas l join spools s on s.linha_id = l.id join itens_necessarios itn on itn.spool_id = s.id join itens i on i.id = itn.item_id)')->get();
//		return View::make('isometricos')->with('isometricos', $isometricos);
		return View::make('emdesenvolvimento');
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		//
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}
}