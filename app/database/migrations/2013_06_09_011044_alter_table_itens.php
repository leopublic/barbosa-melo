<?php

use Illuminate\Database\Migrations\Migration;

class AlterTableItens extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('itens', function($table)
		{
			DB::raw('alter table itens modify unidade_id int unsigned null');
		});	
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
	}

}