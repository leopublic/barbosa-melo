<?php

class StatusSpoolTableSeeder extends Seeder {

    public function run()
    {

        DB::table('status_spool')->delete();

        StatusSpool::create(array(
            'descricao' => 'liberado para construção',
        ));

        StatusSpool::create(array(
            'descricao' => 'em fabricação',
        ));

        StatusSpool::create(array(
            'descricao' => 'fabricado',
        ));

        StatusSpool::create(array(
            'descricao' => 'em pintura',
        ));

        StatusSpool::create(array(
            'descricao' => 'pintado',
        ));

    }

}