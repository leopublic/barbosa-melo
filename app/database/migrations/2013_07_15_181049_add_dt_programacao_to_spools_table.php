<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddDtProgramacaoToSpoolsTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('spools', function(Blueprint $table) {
            $table->datetime('dt_programacao')->nullable();            
			$table->integer('user_id')->unsigned()->nullable();
			$table->integer('status_spool_id')->unsigned()->nullable();			
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('spools', function(Blueprint $table) {
			$table->dropColumn('dt_programacao');
			$table->dropColumn('user_id');
			$table->dropColumn('status_spool_id');
        });
    }

}
