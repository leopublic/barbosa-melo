<?php

use Illuminate\Database\Migrations\Migration;

class CreateTableFornecedores extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('fornecedores', function($table)
		{
			$table->engine = 'InnoDB';
			$table->increments('id');
			$table->string('descricao',500);
			$table->string('cgc',30)->nullable();
			$table->timestamps();
		});			
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('fornecedores');
	}

}