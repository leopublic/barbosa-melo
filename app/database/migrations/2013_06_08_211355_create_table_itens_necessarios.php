<?php

use Illuminate\Database\Migrations\Migration;

class CreateTableItensNecessarios extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('itens_necessarios', function($table)
		{
			$table->engine = 'InnoDB';
			$table->increments('id');
			$table->integer('spool_id')->unsigned()->nullable();
			$table->decimal('qtd',11,2);
			$table->decimal('peso',11,2);
			$table->timestamps();
		});			

		Schema::table('itens_necessarios', function($table)
		{
			$table->foreign('spool_id')->references('id')->on('spools');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
	}

}